import { Component, AfterViewInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { StoreService } from './../store.service';

@Component({
  selector: 'app-gray',
  templateUrl: './gray.component.html',
  styleUrls: ['./gray.component.css']
})
export class GrayComponent implements AfterViewInit {

  @ViewChild('colorselection') elColorSelection: ElementRef;
  @ViewChild('text') elText: ElementRef;

  color: string;
  radius = 10;
  width = 300;
  colorrHeight = 20;
  rgbText: string;
  textColor: string;
  ColorPosX: number;
  ColorField: HTMLCanvasElement;

  @Input() set setColor(color: {r: number, g: number, b: number}) {
    if (typeof color === 'undefined') {
      return;
    }
    const gray = (color.r + color.g + color.b) / 3;
    this.rgbText = String(gray);
    this.store.storeGray(gray);
    this.ColorPosX = ((this.width - (2 * this.radius) - 10) / 255) * gray + this.radius + 5;
  }

  @Output() newColor = new EventEmitter<{r: number, g: number, b: number}>();


  constructor(private store: StoreService) {
    this.ColorPosX = this.radius;
  }

  ngAfterViewInit() {
    this.ColorPosX = this.store.getGrayPosX();
    this.width = this.store.width;
    this.radius = this.store.radius;
    this.ColorField = <HTMLCanvasElement>this.elColorSelection.nativeElement;
    this.ColorField.height = this.colorrHeight;
    this.ColorField.width = this.width;
    this.drawColorField();
  }

  drawColorField() {
    const ctx = this.ColorField.getContext('2d');
    ctx.clearRect(0, 0, this.width, this.colorrHeight);
    const grd = ctx.createLinearGradient(this.radius + 2, 0 , this.width - 2 * this.radius, 0);
    grd.addColorStop(0, 'rgb(255, 255, 255)');
    grd.addColorStop(1, 'rgb(0, 0, 0)');

    ctx.fillStyle = grd;
    ctx.fillRect(this.radius , 5, 280, this.colorrHeight - 10);

    const data = ctx.getImageData(this.ColorPosX, this.colorrHeight / 2, 1, 1).data;
    // const color = new Color([data[0], data[1], data[2]]);

    ctx.beginPath();
    ctx.arc(this.ColorPosX, this.radius, this.radius, 0, 2 * Math.PI);
    ctx.fillStyle = 'rgb(' + data[0] + ',' + data[1] + ',' + data[2] + ')';

    ctx.fill();
    ctx.stroke();

    this.rgbText = data[0].toString();
    if ((data[0] + data[1] + data[2]) / 3 > 128) {
      this.textColor = 'black';
    } else {
      this.textColor = 'white';
    }
    this.color = 'rgb(' + data[0] + ',' + data[1] + ',' + data[2] + ')';
    this.store.storeGray(this.ColorPosX);
    this.newColor.emit({r: Number(data[0]), g: Number(data[1]), b: Number(data[2])});
  }

  hoverColorField(event: MouseEvent) {
    if (event.buttons === 1) {
      const relativePosX = event.clientX - this.ColorField.getBoundingClientRect().left;
      if (relativePosX < (this.radius) || relativePosX >= this.width - this.radius) {
        return;
      }
      this.ColorPosX = relativePosX;
      this.drawColorField();
    }
  }
}
