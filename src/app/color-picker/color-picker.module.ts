import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ColorPickerComponent } from './color-picker/color-picker.component';
import { ColorComponent } from './color/color.component';
import { GrayComponent } from './gray/gray.component';
import { BlackComponent } from './black/black.component';
import { StoreService } from './store.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ColorPickerComponent,
    ColorComponent,
    GrayComponent,
    BlackComponent
  ],
  providers: [StoreService],
  exports: [ColorPickerComponent]
})
export class ColorPickerModule { }
