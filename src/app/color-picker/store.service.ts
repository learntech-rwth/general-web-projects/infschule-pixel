import { Injectable } from '@angular/core';

interface SelectPos {
  x: number;
  y: number;
}

@Injectable()
export class StoreService {
  radius = 10;
  width = 300;
  ColorPosX = this.radius;
  GrayPosX = this.radius;
  selectPos = {x: this.radius, y: this.radius};

  constructor() { }

  getColorPosX(): number {
    return this.ColorPosX;
  }

  getSelectPos(): SelectPos {
    return this.selectPos;
  }

  storeColor(posX: number, selectPos: SelectPos) {
    this.ColorPosX = posX;
    this.selectPos = selectPos;
  }

  getGrayPosX(): number {
    return this.GrayPosX;
  }

  storeGray(posX: number) {
    this.GrayPosX = posX;
  }

}
