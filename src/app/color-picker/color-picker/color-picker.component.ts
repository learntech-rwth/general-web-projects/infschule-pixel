import { Component, OnInit, Input, Output, EventEmitter, HostListener, ViewChild, ElementRef } from '@angular/core';


@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.css']
})
export class ColorPickerComponent implements OnInit {

  firstClick = true;
  color: {r: number, g: number, b: number};

  @Output() close = new EventEmitter<{r: number, g: number, b: number}>();
  @Output() newColor = new EventEmitter<{r: number, g: number, b: number}>();
  @ViewChild('container') elContainer: ElementRef;
  @Input() pickerStyle = 0;

  /**
   * Neue Farbe wird gesetzt. Der ColorPicker gibt diese weiter.
   */
  @Input() set setColor(color: {r: number, g: number, b: number}) {
    if (color === undefined) {
      return;
    }

    this.color = color;
  }

  constructor() { }

  ngOnInit() {
  }

  /**
   * Die gewählte Farbe über den Eventemitter ausgegeben.
   */
  emitColor(color: {r: number, g: number, b: number}) {
    if (color === undefined) {
      return;
    }
    this.color = color;
    this.newColor.emit(color);
  }

  clickClose() {
    this.close.emit(this.color);
  }

  /**
   * Prüft, ob Außerhalb des Pickers geklickt wurde. Wenn dem so ist, schließt er sich.
   */
  @HostListener('document:mousedown', ['$event']) onmousemove(event: MouseEvent) {
    /*if (this.firstClick) {
      this.firstClick = false;
      return true;
    }*/
    if (this.elContainer.nativeElement.contains(<Element>event.target)) {
      return true;
    }
    const offsetTop = this.elContainer.nativeElement.getBoundingClientRect().top;
    const offsetLeft = this.elContainer.nativeElement.getBoundingClientRect().left;
    const offsetHeight = this.elContainer.nativeElement.offsetHeight;
    const offsetWidth = this.elContainer.nativeElement.offsetWidth;
    if (event.clientX < offsetLeft || event.clientX > offsetLeft + offsetWidth) {
      this.close.emit(this.color);
      this.firstClick = true;
      return true;
    }
    if (event.clientY < offsetTop || event.clientY > offsetTop + offsetHeight) {
      this.close.emit(this.color);
      this.firstClick = true;
      return true;
    }
  }
}
