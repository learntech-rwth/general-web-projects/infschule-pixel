import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-black',
  templateUrl: './black.component.html',
  styleUrls: ['./black.component.css']
})
export class BlackComponent implements OnInit {

  @Output() newColor = new EventEmitter<{r: number, g: number, b: number}>();

  constructor() { }

  ngOnInit() {
  }

  cklickWhite() {
    this.newColor.emit({r: 255, g: 255, b: 255});
  }

  cklickBlack() {
    this.newColor.emit({r: 0, g: 0, b: 0});
  }

}
