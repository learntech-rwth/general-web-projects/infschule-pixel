import { Component, AfterViewInit, Input, ViewChild, ElementRef, Output, EventEmitter} from '@angular/core';
import { StoreService } from './../store.service';

interface SelectPos {
  x: number;
  y: number;
}

@Component({
  selector: 'app-color',
  templateUrl: './color.component.html',
  styleUrls: ['./color.component.css']
})
/**
 * Die Variante für die Farbeingabe.
 */
export class ColorComponent implements AfterViewInit {
  @ViewChild('selectionfield') elSelectionField: ElementRef;
  @ViewChild('colorselection') elColorSelection: ElementRef;
  @ViewChild('text') elText: ElementRef;

  curColor: {r: number, g: number, b: number};
  colorString: string;
  radius = 10;
  width = 300;
  selectHeight = 150;
  colorrHeight = 20;
  rgbText: string;
  textColor: string;
  ColorPosX: number;
  ColorField: HTMLCanvasElement;
  SelectField: HTMLCanvasElement;
  selectPos: SelectPos;

  /**
   * Bei einer neuen Farbe, werden die HSV Farben ausgerechnet, um die Position im Canvas zubestimmen.
   */
  @Input() set setColor(color: {r: number, g: number, b: number}) {
    if (typeof color === 'undefined') {
      return;
    }
    this.curColor = color;
    const hsv = this.getHSV();
    const xPos = (this.width - 10) * hsv.s + 5;
    const yPos = (this.selectHeight - 10) * (1 - hsv.v) + 5;
    this.selectPos = {x: xPos, y: yPos};
    this.ColorPosX = ((this.width - (2 * this.radius) - 10) / 360) * hsv.h + this.radius + 5;
    this.rgbText = color.r + ' ' + color.g + ' ' + color.b;
    this.store.storeColor(this.ColorPosX, this.selectPos);
  }

  @Output() newColor = new EventEmitter<{r: number, g: number, b: number}>();

  constructor(private store: StoreService) {
    this.ColorPosX = (300 / 255) * 153;
    this.selectPos = {x: this.radius, y: this.radius};
  }

  ngAfterViewInit() {
    this.SelectField = <HTMLCanvasElement>this.elSelectionField.nativeElement;
    this.SelectField.height = this.selectHeight;
    this.SelectField.width = this.width;
    this.ColorField = <HTMLCanvasElement>this.elColorSelection.nativeElement;
    this.ColorField.height = this.colorrHeight;
    this.ColorField.width = this.width;
    this.radius = this.store.radius;
    this.selectPos = this.store.getSelectPos();
    this.ColorPosX = this.store.getColorPosX();
    this.width = this.store.width;
    this.drawColorField();
  }

  /**
   * Zeichnet das Große Selektionsfeld
   */
  drawSelectionField(r: number, g: number, b: number) {
    const ctx = this.SelectField.getContext('2d');
    ctx.clearRect(0, 0, this.width, this.selectHeight);

    let grd = ctx.createLinearGradient(5, 0, this.width - 5, 0);
    grd.addColorStop(0, 'rgb(255, 255, 255)');
    grd.addColorStop(1, 'rgb(' + r + ', ' + g + ', ' + b + ')');

    ctx.fillStyle = grd;
    ctx.fillRect(0, 0, this.width, this.selectHeight);

    grd = ctx.createLinearGradient(0, this.selectHeight - 5, 0, 5);
    grd.addColorStop(0, 'rgba(0, 0, 0, 1)');
    grd.addColorStop(1, 'rgba(0, 0, 0, 0)');

    ctx.fillStyle = grd;
    ctx.fillRect(0, 0, this.width, this.selectHeight);

    ctx.beginPath();
    ctx.arc(this.selectPos.x, this.selectPos.y, this.radius, 0, 2 * Math.PI);

    const data = ctx.getImageData(this.selectPos.x, this.selectPos.y, 1, 1).data;
    ctx.fillStyle = 'rgb(' + data[0] + ',' + data[1] + ',' + data[2] + ')';
    this.colorString = 'rgb(' + data[0] + ',' + data[1] + ',' + data[2] + ')';

    ctx.fill();
    ctx.stroke();
    if ((data[0] + data[1] + data[2]) / 3 > 128 || data[1] > 192) {
      this.textColor = 'black';
    } else {
      this.textColor = 'white';
    }
    this.store.storeColor(this.ColorPosX, this.selectPos);
  }

  /**
   * Zeichnet das kleine Selektionsfeld
   */
  drawColorField() {
    const ctx = this.ColorField.getContext('2d');
    ctx.clearRect(0, 0, this.width, this.colorrHeight);
    const grd = ctx.createLinearGradient(this.radius + 5, 0 , this.width - this.radius - 5, 0);
    grd.addColorStop(0, 'rgb(255, 0, 0)');
    grd.addColorStop((1 / 6), 'rgb(255, 255, 0)');
    grd.addColorStop((2 / 6), 'rgb(0, 255, 0)');
    grd.addColorStop((3 / 6), 'rgb(0, 255, 255)');
    grd.addColorStop((4 / 6), 'rgb(0, 0, 255)');
    grd.addColorStop((5 / 6), 'rgb(255, 0, 255)');
    grd.addColorStop(1, 'rgb(255, 0, 0)');

    ctx.fillStyle = grd;
    ctx.fillRect(this.radius , 5, 280, this.colorrHeight - 10);

    const data = ctx.getImageData(this.ColorPosX, this.colorrHeight / 2, 1, 1).data;
    ctx.beginPath();
    ctx.arc(this.ColorPosX, this.radius, this.radius, 0, 2 * Math.PI);
    ctx.fillStyle = 'rgb(' + data[0] + ',' + data[1] + ',' + data[2] + ')';
    ctx.fill();
    ctx.stroke();
    this.drawSelectionField(data[0], data[1], data[2]);
  }

  /**
   * Wird aufgerufen, wenn der Wert der kleinen Selektion verändert wurde.
   */
  hoverColorField(event: MouseEvent) {
    if (event.buttons === 1) {
      const relativePosX = event.clientX - this.ColorField.getBoundingClientRect().left;
      if (relativePosX < (this.radius) || relativePosX >= this.width - this.radius) {
        return;
      }
      this.ColorPosX = relativePosX;
      this.drawColorField();
      const ctx = this.SelectField.getContext('2d');
      const data = ctx.getImageData(this.selectPos.x, this.selectPos.y, 1, 1).data;
      this.rgbText = data[0] + ' ' + data[1] + ' ' + data[2];
      this.curColor = {r: Number(data[0]), g: Number(data[1]), b: Number(data[2])};
      this.newColor.emit({r: Number(data[0]), g: Number(data[1]), b: Number(data[2])});
    }
  }

  /**
   * Wird aufgerufen, wenn der Wert der großen Selektion aufgerufen wurde.
   */
  hoverSelectField(event: MouseEvent) {
    if (event.buttons === 1) {
      const relativePosX = event.clientX - this.SelectField.getBoundingClientRect().left;
      if (relativePosX > this.width - 2) {
        return;
      }
      const relativePosY = event.clientY - this.SelectField.getBoundingClientRect().top;
      this.selectPos = {x: relativePosX, y: relativePosY};
      this.drawColorField();
      const ctx = this.SelectField.getContext('2d');
      const data = ctx.getImageData(this.selectPos.x, this.selectPos.y, 1, 1).data;
      this.rgbText = data[0] + ' ' + data[1] + ' ' + data[2];
      this.curColor = {r: Number(data[0]), g: Number(data[1]), b: Number(data[2])};
      this.newColor.emit({r: Number(data[0]), g: Number(data[1]), b: Number(data[2])});
    }
  }

  drawCircle() {

  }

  getHSV(): {h: number, s: number, v: number} {
    const r = this.curColor.r / 255;
    const g = this.curColor.g / 255;
    const b = this.curColor.b / 255;
    const max = Math.max(r, g, b);
    const min = Math.min(r, g, b);

    const h = this.getH(r, g, b, min, max);
    const s = this.getS(min, max);
    const v = max;
    return {h: h, s: s, v: v};
  }

  getS(min: number, max: number): number {
    if (max === 0) {
      return 0;
    }
    return (max - min) / max;
  }

  getH(r: number, g: number, b: number, min: number, max: number): number {
    let h = 0;
    const delta = max - min;

    if (delta === 0) {
      return 0;
    }

    if (max === r) {
      h = 60 * ((g - b) / delta);
    } else if (max === g) {
      h = 60 * (2 + (b - r) / delta);
    } else if (max === b) {
      h = 60 * (4 + (r - g) / delta);
    }

    if (h < 0) {
      return h += 360;
    }
    return h;
  }
}
