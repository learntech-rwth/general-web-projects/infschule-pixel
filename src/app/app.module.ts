import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LeftToolbarComponent } from './left-toolbar/left-toolbar.component';

import { PixelEditorModule } from './pixel-editor/pixel-editor.module';


@NgModule({
  imports: [
    BrowserModule,
    PixelEditorModule
  ],
  declarations: [
    AppComponent,
    LeftToolbarComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
