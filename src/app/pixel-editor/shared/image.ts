import { ColorElement } from './color-element';

/**
 * Daten Objekt für das Bild
 *
 * Der richtige Umgang mit dem Objekt wird erwartet.
 *
 * Die größe des Array darf nicht geändert werden.
 */

export class Image {
  name: string;
  format: number;
  pixelList: ColorElement[][];

/**
 * Der Konstrukor initialisiert direkt den Array mit weißern Farb Elementen
 */
  constructor(private height: number, private width: number) {
    this.pixelList = new Array(this.width);
    for (let i = 0; i < this.width; i++) {
      this.pixelList[i] = new Array(this.height);
      for (let j = 0; j < this.height; j++) {
        this.pixelList[i][j] = new ColorElement(255, 255, 255);
      }
    }
    this.name = '';
    this.format = 0;
  }

/**
 * getter
 */
  getheight(): number {
    return this.height;
  }

  getwidth(): number {
    return this.width;
  }
}
