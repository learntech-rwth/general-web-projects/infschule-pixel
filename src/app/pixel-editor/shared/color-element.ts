/**
 * Verwaltet einen einigen Pixel und stell Funtionen dafür bereit.
*/
export class ColorElement {
  constructor(private red: number, private green: number, private blue: number) {
    if (this.red > 255 || this.red < 0) {
      this.red = 255;
    }
    if (this.green > 255 || this.green < 0) {
      this.green = 255;
    }
    if (this.blue > 255 || this.blue < 0) {
      this.blue = 255;
    }
  }

  zeroPadding(number: Number): string {
    let stringNumber = number.toString();
    while (stringNumber.length < 3) {
      stringNumber = '0' + stringNumber;
    }
    return stringNumber;
  }

  blankPadding(number: Number): string {
    let stringNumber = number.toString();
    while (stringNumber.length < 3) {
      stringNumber = '0' + stringNumber;
    }
    return stringNumber;
  }

  /* setColorRed(red: number) {
    this.red = red;
  }

  setColorGreen(green: number) {
    this.green = green;
  }

  setColorBlue(blue: number) {
    this.blue = blue;
  } */

  setColor(red: number, green: number, blue: number) {
    this.red = red;
    this.green = green;
    this.blue = blue;
  }

  setGray(grey: number) {
    this.red = grey;
    this.green = grey;
    this.blue = grey;
  }

  setBlack(black: boolean) {
    if (black) {
      this.red = 0;
      this.green = 0;
      this.blue = 0;
    } else {
      this.red = 255;
      this.green = 255;
      this.blue = 255;
    }
  }

  // Todo
  getBlack(): string {
    if (this.red < 128 || this.green < 128 || this.blue < 128) {
      return this.toRgbString(0, 0, 0);
    }
    return this.toRgbString(255, 255, 255);
  }

  getGray(): string {
    const gray = Math.floor((Math.floor((Number(this.red) + Number(this.green) + Number(this.blue)) / 3)));
    return this.toRgbString(gray, gray, gray);
  }

  getRGB(): string {
    return this.toRgbString(this.red, this.green, this.blue);
  }

  getBlackText(): string {
    if (this.red < 128 || this.green < 128 || this.blue < 128) {
      return '1';
    }
    return '0';
  }

  getGrayText(): string {
    return this.blankPadding(Math.floor((Number(this.red) + Number(this.green) + Number(this.blue)) / 3));
  }

  getColorText(): string {
    return this.zeroPadding(this.red) + ' ' + this.zeroPadding(this.green) + ' ' + this.zeroPadding(this.blue);
  }

  toRgbString(r: number, g: number, b: number): string {
    return 'rgb(' + r + ',' + g + ',' + b + ')';
  }

  getColor(): any {
    return {red: this.red, green: this.green, blue: this.blue};
  }
}
