import { EventBase } from './event-base';
/**
 * Event zur Mitteilung, dass der source Code geänder wurde.
 * Das Event enthält, die Position und die neue Farbe
 */
export class SrcChangeEvent extends EventBase {
  mode: number;
  PosX: number;
  PosY: number;
  red: number;
  green: number;
  blue: number;
  payload: any;
  name: any;
  state: number;
  constructor(PosX: number = 0, PosY: number = 0, red: number = 0, green: number = 0, blue: number = 0) {
    super(1);
    this.mode = 0;
    this.PosX = PosX;
    this.PosY = PosY;
    this.red = red;
    this.green = green;
    this.blue = blue;
    this.state = 0;
  }
}
