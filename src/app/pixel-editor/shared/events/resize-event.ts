import { EventBase } from './event-base';
export class ResizeEvent extends EventBase {
  width: number;
  height: number;
  constructor(width: number = 0, height: number = 0, public isRelative = true) {
    super(0);
    this.width = width;
    this.height = height;
  }

  setSize(width: number, height: number) {
    this.width = width;
    this.height = height;
  }
}
