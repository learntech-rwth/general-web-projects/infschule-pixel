import { EventBase } from './event-base';
/**
 * Event zur Mitteilung, dass das Bild geänder wurde.
 * Das Event enthält, die Position und die von da ausgehende Rechtecke
 */

interface Color {
  r: number;
  g: number;
  b: number;
}

export class ImgChangeEvent extends EventBase {
  name: String;
  PosX: number;
  PosY: number;
  width: number;
  height: number;
  color: Color;
  state: number; // 0 = start, 1 = stream, 2 = end, 3 = name
  constructor(PosX: number, PosY: number, width: number = 1, height: number = 1, ) {
    super(1);
    this.PosX = PosX;
    this.PosY = PosY;
    this.width = width;
    this.height = height;
    this.color = {r: 255, g: 255, b: 255};
  }
}
