import { ColorElement } from './color-element';

export class ColorPixel {
  constructor(public x: number, public y: number, public color: ColorElement) {
  }
}
