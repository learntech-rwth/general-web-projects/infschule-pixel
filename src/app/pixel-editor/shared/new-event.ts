export class NewEvent {
  constructor(public valid: boolean,
    public name: string = '',
    public height: number = 0,
    public width: number = 0,
    public format: number = 0) {}
}
