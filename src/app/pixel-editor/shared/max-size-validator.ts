import { AbstractControl, Validator, ValidatorFn, Validators } from '@angular/forms';

export function maxSizeValidator(maxSize: number): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    if (control.value <= maxSize && control.value > 0) {
      return null;
    }
    return {'maxSize': {value: control.value}};
  };
}

export function colorInputValidator(maxSize: number): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    if (!/[0-9]+/.test(String(control.value))) {
      return {'grayInput': {value: control.value}};
    }
    if (Number(control.value) < 0 || Number(control.value) > maxSize) {
      return {'grayInput': {value: control.value}};
    }
    return null;
  };
}

export function blackInputValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    if (Number(control.value) === 1 || Number(control.value) === 0) {
      return null;
    }
    return {'blackInput': {value: control.value}};
  };
}
