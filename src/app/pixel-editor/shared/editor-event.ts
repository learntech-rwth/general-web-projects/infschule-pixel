export class EditorEvent {
    eventName: string;
    controller: boolean;
    eventNumber: number;
    payload: any;
}
