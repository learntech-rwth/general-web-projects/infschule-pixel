import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OverlayNewComponent } from './overlay-new.component';

describe('OverlayNewComponent', () => {
  let component: OverlayNewComponent;
  let fixture: ComponentFixture<OverlayNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlayNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
