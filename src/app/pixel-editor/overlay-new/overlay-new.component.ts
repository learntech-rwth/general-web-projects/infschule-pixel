import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NewEvent } from '../shared/new-event';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { maxSizeValidator } from '../shared/max-size-validator';
import { ConfigStorageService } from '../services/config-storage.service';

@Component({
  selector: 'app-overlay-new',
  templateUrl: './overlay-new.component.html',
  styleUrls: ['./overlay-new.component.css']
})
/**
 * Das Konfigurationsfenser für ein neues Bild. Stellt ein Formular mit einer FormGroup zur verfügung,
 * welches mit entsprechenden Validatoren geladen wird. Über den Eventemitter werden die Informationen dann
 * übergeben.
 */
export class OverlayNewComponent implements OnInit {
  formats = ['PPM', 'PGM', 'PBM'];
  newImg = {name: 'Bildname', width: '10', height: '10', format: this.formats[0]};
  newForm: FormGroup;

  @Output() newEvent = new EventEmitter<NewEvent>();
  constructor(public config: ConfigStorageService) { }

  ngOnInit() {
    this.newForm = new FormGroup({
      'imgName': new FormControl(this.config.overlay.initialName, [
        Validators.required,
        Validators.pattern('[^\\\\|\/|\:|\*|\?|\"|\<|\>|\|]+')
      ]),
      'width': new FormControl(this.config.overlay.initialWidth, maxSizeValidator(this.config.overlay.maxWidth)),
      'height': new FormControl(this.config.overlay.initialHeight, maxSizeValidator(this.config.overlay.maxHeight)),
      'format': new FormControl(this.formats[0])
    });
  }

  public overlayOff() {
    const event = new NewEvent(false);
    this.newEvent.emit(event);
  }

  register(value: any) {
    console.log(value.format);
    const event = new NewEvent(true, value.imgName, value.width, value.height, Number(this.formats.indexOf(value.format)));
    this.newEvent.emit(event);
  }

  get imgName() {
    return this.newForm.get('imgName');
  }

  get width() {
    return this.newForm.get('width');
  }

  get height() {
    return this.newForm.get('height');
  }
}
