import { Component, OnInit, Output, EventEmitter, Input, HostListener, ViewChild, ElementRef} from '@angular/core';
import { EditorEvent } from '../shared/editor-event';
import { FormControl, FormGroup } from '@angular/forms';
import { ConfigStorageService } from '../services/config-storage.service';

@Component({
  selector: 'app-top-toolbar',
  templateUrl: './top-toolbar.component.html',
  styleUrls: ['./top-toolbar.component.css']
})
/**
 * Die Toolbar, stellt die Buttons zur verfügung und teilt bei einem Druch dauf, dies der
 * Parent-Komponente mit.
 */
export class TopToolbarComponent {

  selectedColor: string;
  selectedValue: number;
  toolbarForm: FormGroup;
  srcViewBtnColor: string;
  imgViewBtnColor: string;
  gridBtnColor: string;
  srcViewActive = true;
  imgViewActive = true;
  gridActive = true;
  isInit = false;
  formats = ['PPM', 'PGM', 'PBM'];
  displayTools = false;
  displayFullscreen = false;
  toolsImg = './assets/pencil-tools.svg';
  paintBoard = './assets/paint-board.svg';

  @ViewChild('tools') elTools: ElementRef;
  @ViewChild('fullscreen') elFullscreen: ElementRef;

  @Output() toolbarEvent = new EventEmitter<EditorEvent>();

  /**
   * Der Toolbar wird die gewählte Farbe mitgeteilt.
   * Die Farbwahl, geschieht über ein anderes Modul. Die Toolbar teilt nur den Buttondruck mit.
   */
  @Input() set newColor(color: {r: number, g: number, b: number}) {
    if (color === undefined) {
      return;
    }
    if ((color.r + color.g + color.b) / 3 > 96 || color.g > 192) {
      this.paintBoard = './assets/paint-board.svg';
    } else {
      this.paintBoard = './assets/paint-board-white.svg';
    }
    this.selectedColor = 'rgb(' + Math.floor(color.r) + ',' + Math.floor(color.g) + ',' + Math.floor(color.b) + ')';
  }

  /**
   * Die ToolbarInfo teilt, der Toolbar mit, ob der Editor mit einem Bild initialisiert wurde und
   * wenn ja, welches Format gewählt wurde.
   */
  @Input() set toolbarInfo(info: {isInit: boolean, format: number}) {
    if (info.isInit === false) {
      return;
    }
    this.isInit = info.isInit;
    this.toolbarForm.controls['format'].setValue(info.format);
  }

  constructor(public config: ConfigStorageService) {
    this.selectedColor = 'rgb(255,255,255)';
    this.srcViewBtnColor = this.config.toolbar.toggleColor;
    this.imgViewBtnColor = this.config.toolbar.toggleColor;
    this.gridBtnColor = this.config.toolbar.toggleColor;
    this.toolbarForm = new FormGroup({'format': new FormControl(this.formats[0])});
    this.toolbarForm.controls['format'].valueChanges.subscribe((value) => {
      this.changeFormat(value);
    });
  }

  /*---------------------------------------------------------------------------------------------------------------------
   * Für Jeden Button, wird ein Event über den gleichen EventEmitter geschickt. Jeder Button hat eine eigene eventNumber.
   ---------------------------------------------------------------------------------------------------------------------- */

  clickNew() {
    const event: EditorEvent = new EditorEvent();
    event.controller = true;
    event.eventName = 'new';
    event.eventNumber = 1;
    this.toolbarEvent.emit(event);
  }

  clickSave() {
    const event: EditorEvent = new EditorEvent();
    event.controller = true;
    event.eventName = 'save';
    event.eventNumber = 2;
    this.toolbarEvent.emit(event);
  }

  clickLoad(content: any) {
    const event: EditorEvent = new EditorEvent();
    event.controller = true;
    event.eventName = 'load';
    event.eventNumber = 3;
    event.payload = content.target.files[0];
    this.toolbarEvent.emit(event);
  }

  clickFullScreen() {
    const event: EditorEvent = new EditorEvent();
    event.controller = true;
    event.eventName = 'full screen';
    event.eventNumber = 4;
    this.toolbarEvent.emit(event);
  }

  clickColor(event: MouseEvent) {
    const eEvent: EditorEvent = new EditorEvent();
    eEvent.controller = true;
    eEvent.eventName = 'color';
    eEvent.eventNumber = 5;
    eEvent.payload = event;
    this.toolbarEvent.emit(eEvent);
  }

  clickSwitch(content: any) {
  }

  changeFormat(value: any) {
    const event: EditorEvent = new EditorEvent();
    event.controller = true;
    event.eventName = 'color';
    event.eventNumber = 6;
    event.payload = Number(value);
    this.toolbarEvent.emit(event);
  }

  toggleImg(content: any) {
    console.log('toggle img');
    this.imgViewActive = content.target.checked;
    const event: EditorEvent = new EditorEvent();
    event.controller = true;
    event.eventName = 'toggleImg';
    event.eventNumber = 7;
    event.payload = this.imgViewActive;
    this.toolbarEvent.emit(event);
  }

  toggleSrc(content: any) {
    console.log('toggle source');
    this.srcViewActive = content.target.checked;
    const event: EditorEvent = new EditorEvent();
    event.controller = true;
    event.eventName = 'toggleSrc';
    event.eventNumber = 8;
    event.payload = this.srcViewActive;
    this.toolbarEvent.emit(event);
  }

  toggleGrid(content: any) {
    this.gridActive = content.target.checked;
    const event: EditorEvent = new EditorEvent();
    event.controller = true;
    event.eventName = 'toggleGrid';
    event.eventNumber = 9;
    event.payload = this.gridActive;
    this.toolbarEvent.emit(event);
  }

  fullscreenImg(content: any) {
    const event: EditorEvent = new EditorEvent();
    event.controller = true;
    event.eventName = 'fullscreenImg';
    event.eventNumber = 10;
    this.toolbarEvent.emit(event);
  }

  fullscreenSrc(content: any) {
    const event: EditorEvent = new EditorEvent();
    event.controller = true;
    event.eventName = 'fullscreenSrc';
    event.eventNumber = 11;
    this.toolbarEvent.emit(event);
  }

  clickPencil() {
    this.toolsImg = './assets/pencil-black.svg';
    const event: EditorEvent = new EditorEvent();
    event.controller = true;
    event.eventName = 'pencil';
    event.eventNumber = 12;
    this.toolbarEvent.emit(event);
  }

  clickRectangle() {
    this.toolsImg = './assets/rectangle-black.svg';
    const event: EditorEvent = new EditorEvent();
    event.controller = true;
    event.eventName = 'rectangle';
    event.eventNumber = 13;
    this.toolbarEvent.emit(event);
  }

  clickRubber() {
    this.toolsImg = './assets/rubber-black.svg';
    const event: EditorEvent = new EditorEvent();
    event.controller = true;
    event.eventName = 'rubber';
    event.eventNumber = 14;
    this.toolbarEvent.emit(event);
  }

  clickPipette() {
    this.toolsImg = './assets/pipette-black.svg';
    const event: EditorEvent = new EditorEvent();
    event.controller = true;
    event.eventName = 'pipette';
    event.eventNumber = 15;
    this.toolbarEvent.emit(event);
  }

  clickUndo() {
    const event: EditorEvent = new EditorEvent();
    event.controller = true;
    event.eventName = 'undo';
    event.eventNumber = 16;
    this.toolbarEvent.emit(event);
  }

  clickRedo() {
    const event: EditorEvent = new EditorEvent();
    event.controller = true;
    event.eventName = 'redo';
    event.eventNumber = 17;
    this.toolbarEvent.emit(event);
  }

  toggle(value: boolean): boolean {
    if (value) {
      return false;
    }
    return true;
  }

  showTools() {
    if (this.displayTools) {
      this.displayTools = false;
    } else {
      this.displayTools = true;
    }
  }

  showFullscreen() {
    if (this.displayFullscreen) {
      this.displayFullscreen = false;
    } else {
      this.displayFullscreen = true;
    }
  }

  @HostListener('document:click', ['$event']) onkeydown(event: MouseEvent) {
    if (this.elTools.nativeElement.contains(<Element>event.target)) {
      this.displayFullscreen = false;
      return;
    }
    if (this.elFullscreen.nativeElement.contains(<Element>event.target)) {
      this.displayTools = false;
      return;
    }
    this.displayFullscreen = false;
    this.displayTools = false;
  }
}
