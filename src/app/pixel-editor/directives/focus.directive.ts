import { Directive, Input, EventEmitter, ElementRef, Renderer, Inject} from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Directive({
  selector: '[appFocus]'
})
export class FocusDirective implements OnInit {
  @Input('appFocus') appFocus: EventEmitter<boolean>;

  constructor(private element: ElementRef, private renderer: Renderer) {
  }

  ngOnInit() {
    this.appFocus.subscribe(event => {
      this.element.nativeElement.focus();
      this.renderer.invokeElementMethod(this.element.nativeElement, 'focus', []);
      this.element.nativeElement.value = '';
    });
  }
}
