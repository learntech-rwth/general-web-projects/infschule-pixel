import { Component, OnInit, Output, AfterViewInit, Input, EventEmitter, ElementRef, ViewChild, HostListener} from '@angular/core';
import { ImgRenderControllerService } from '../services/img-render-controller.service';
import { EventBase } from '../shared/events/event-base';
import { ResizeEvent } from '../shared/events/resize-event';
import { Image } from '../shared/image';

@Component({
  selector: 'app-img-view',
  templateUrl: './img-view.component.html',
  styleUrls: ['./img-view.component.css']
})

/**
 * Die Komponente, welche zuständig ist für die Bild-Ansicht des Bildes. Die Logik befindet sich
 * im Render-Service. Hier werden die Eingaben und registriert und das Aussehen gesteuert
 */
export class ImgViewComponent implements AfterViewInit {

  height: string;
  width: string;
  canvas: HTMLCanvasElement;
  pixelW: number;
  pixelH: number;
  isImg = false;
  isViewInit = false;
  resizeActive = false;

  @Output() eventTrigger = new EventEmitter<EventBase>();

  @ViewChild('imgcanvas') elCanvas: ElementRef;
  @ViewChild('container') elContainer: ElementRef;

  /**
   * Notwendige Referenzen werden dem Constructor übergeben.
   * @param {ElementRef} elRef Die Referenz auf dieses Objekt.
   * @param {ImgRenderControllerService} renderer Der Rendererer für das Bild.
   */
  constructor(private elRef: ElementRef, private renderer: ImgRenderControllerService) {
    this.height = this.renderer.imgHeight + 'px';
    this.width = this.renderer.imgWidth + 'px';
  }

  @Input() cursor: string;

  /**
   * Setzt die geänderte Größe in der Bild-Ansicht um.
   * @param {ResizeEvent} size Die geänderte Größe.
   */
  @Input() set newSize(size: ResizeEvent) {
    if (!size) {
      return;
    }

    if (size.isRelative) {
      this.renderer.imgHeight += size.height;
      this.renderer.imgWidth += size.width;
      this.renderer.canvasResize();
    } else {
      this.renderer.imgWidth = size.width;
      const resize = this.renderer.canvasResize();
      if (resize !== undefined) {
        this.renderer.imgHeight += resize.height;
      }
    }
    if (this.renderer.imgWidth < this.renderer.minViewWidth) {
      this.renderer.imgWidth = this.renderer.minViewWidth;
      const resize = this.renderer.canvasResize();
      if (resize !== undefined) {
        this.renderer.imgHeight += resize.height;
      }
    }
    this.height = this.renderer.imgHeight + 'px';
    this.width = this.renderer.imgWidth + 'px';
    this.renderer.drawGrid();
  }

  /**
   * Initialisiert bei einem neuen Bild die Komponente.
   * @param {Image} img Das neue Bild.
   */
  @Input() set newImg(img: Image) {
    if (!img) {
      return;
    }
    const canvas = <HTMLCanvasElement>this.elCanvas.nativeElement;
    if (this.isViewInit) {
      this.eventTrigger.emit(this.renderer.init(canvas, img));
    } else {
      this.renderer.init(canvas, img);
    }
    // this.renderer.init(canvas, img);
    this.renderer.drawGrid();
  }

  /**
   * Es kann erst in den Canvas-Elementen gezeichnet werden, wenn die Größe umgesetzt wurde.
   */
  ngAfterViewInit() {
    this.renderer.canvasResize();
    this.renderer.drawGrid();
    this.isViewInit = true;
  }

  /**
   * Reagiert auf Mausbewegungen beim Zeichnen.
   */
  @HostListener('mousemove', ['$event']) onMouseMove(event: MouseEvent) {
    if (this.resizeActive) {
      return;
    }
    if (!this.renderer.checkNewField(event)) { // Prüft, ob die Maus über einem anderen Feld ist.
      return;
    }
    // Radiergummi oder Pinsel, bei gedrückter Maustaste
    if (event.buttons === 1 && (this.renderer.getToolmode() === 0 || this.renderer.getToolmode() === 2)) {
      this.renderer.setCurRect(event);
      const changeEvent = this.renderer.getImgChangeEvent(event);
      if (!changeEvent) {
        return;
      }
      changeEvent.state = 1;
      this.eventTrigger.emit(changeEvent);
      this.renderer.drawGrid();
      this.renderer.rectSelect(event);
    } else if (event.buttons === 1 && this.renderer.getToolmode() === 1) { // Rechteck bei gedrückter Maustaste.
      const changeEvent = this.renderer.getImgChangeEvent(event);
      if (!changeEvent) {
        return;
      }
      changeEvent.state = 1;
      this.eventTrigger.emit(changeEvent);
      this.renderer.drawGrid();
      this.renderer.drawSelectRectangle(event);
    } else { // Alle anderen Fälle. Selektionsfenster wird verschoben.
      this.renderer.setCurRect(event);
      this.renderer.rectSelect(event);
    }
  }

  /**
   * Reagiert wenn die Maus gedrückt wird. Beim Zeichnen oder Rechteck ziehen.
   */
  @HostListener('mousedown', ['$event']) onMouseDown(event: MouseEvent) {
    if (this.renderer.getToolmode() === 1) {
      const changeEvent = this.renderer.startRectangle(event);
      if (!changeEvent) {
        return;
      }
      this.eventTrigger.emit(changeEvent);
      this.renderer.drawGrid();
    } else if (this.renderer.getToolmode() === 3) {
      const changeEvent = this.renderer.getColor(event);
      changeEvent.state = 0;
      this.eventTrigger.emit(changeEvent);
    } else {
      const changeEvent = this.renderer.getImgChangeEvent(event);
      if (!changeEvent) {
        return;
      }
      changeEvent.state = 0;
      this.eventTrigger.emit(changeEvent);
      this.renderer.drawGrid();
    }
  }

  /**
   * Notwendige Referenzen werden dem Constructor übergeben.
   */
  @HostListener('mouseup', ['$event']) onMouseUp(event: MouseEvent) {
    if (this.renderer.getToolmode() === 1) {
      this.renderer.endRectangle();
      this.renderer.drawGrid();
      this.renderer.drawSelect();
    }
  }

  /**
   * Reagiert darauf, wenn die Maus Das Canvas verlässt.
   */
  @HostListener('mouseleave', ['$event']) onMouseleave(event: MouseEvent) {
    if (event.buttons === 1 && this.renderer.getToolmode() === 1) { // Nicht wenn gerade ein Rechteck gezogen wird
      return;
    }
    this.renderer.mouseLeave();
    this.renderer.drawGrid();
  }

  /**
   * Teilt der Komponente mit, ob gerade eine Größenänderung Stattfindet.
   * Es wird solange nicht bei einem MausHover gezeichnet, damit man beim Verkleinert nicht das Bild übermalt.
   * @param {boolean} resizeActive Gibt an, ob das Ereigniss gerade anfängt oder aufhört.
   */
  setResize(resizeActive: boolean) {
    this.resizeActive = resizeActive;
  }

  /**
   * Teilt dem Controller mit, dass die Größe geändert wird
   * @param {ResizeEvent} event Die Referenz auf dieses Objekt.
   */
  resize(event: ResizeEvent) {
    const x = Math.max(event.width, event.height);
    this.renderer.adjustSize(event);
    this.eventTrigger.emit(event);
  }
}
