import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TopToolbarComponent } from './top-toolbar/top-toolbar.component';
import { ImgViewComponent } from './img-view/img-view.component';
import { SrcViewComponent } from './src-view/src-view.component';
import { ResizeComponent } from './resize/resize.component';
import { ControllerService } from './services/controller.service';
import { ImgRenderControllerService } from './services/img-render-controller.service';
import { SrcRenderControllerService } from './services/src-render-controller.service';
import { ConfigStorageService } from './services/config-storage.service';
import { FileManagerService } from './services/file-manager.service';
import { OverlayNewComponent } from './overlay-new/overlay-new.component';
import { FocusDirective } from './directives/focus.directive';
import { EditorComponent } from './editor/editor.component';
import { HttpModule } from '@angular/http';

import { ColorPickerModule } from '../color-picker/color-picker.module';

import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ColorPickerModule,
    ReactiveFormsModule,
    HttpModule
  ],
  declarations: [
    TopToolbarComponent,
    ImgViewComponent,
    SrcViewComponent,
    ResizeComponent,
    OverlayNewComponent,
    FocusDirective,
    EditorComponent
  ],
  providers: [ControllerService,
    ImgRenderControllerService,
    SrcRenderControllerService,
    ConfigStorageService,
    FileManagerService
  ],
  exports: [EditorComponent]
})
export class PixelEditorModule { }
