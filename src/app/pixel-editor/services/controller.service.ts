import { Injectable } from '@angular/core';
import { EditorEvent } from '../shared/editor-event';
import { ResizeEvent } from '../shared/events/resize-event';

// custom Services
import { SrcRenderControllerService } from '../services/src-render-controller.service';
import { ImgRenderControllerService } from '../services/img-render-controller.service';
import { FileManagerService } from '../services/file-manager.service';
import { ConfigStorageService } from '../services/config-storage.service';

// custom classes
import { ColorElement } from '../shared/color-element';
import { ColorPixel } from '../shared/color-pixel';
import { Image } from '../shared/image';
import { ImgChangeEvent } from '../shared/events/img-change-event';
import { SrcChangeEvent } from '../shared/events/src-change-event';
import { NewEvent } from '../shared/new-event';
import { Observable } from 'rxjs/Observable';

/**
 * Im Controller-Service laufen alle Daten, die auswirkungen auf andere Komponenten haben, zusammen.
 * Nur hier werden die Bilddaten verändert.
 * @param {ResizeEvent} imgSize Renderer der ASCII-Ansicht
 * @param {ResizeEvent} srcSize Renderer der ASCII-Ansicht
 */
@Injectable()
export class ControllerService {

  imgSize: ResizeEvent = new ResizeEvent;
  srcSize: ResizeEvent = new ResizeEvent;

  private minSize: number;
  private container: HTMLElement;
  private content: HTMLElement;
  toolmode = 0;
  color: ColorElement;
  isInit = false;
  undoStates: Map<string, ColorPixel>[];
  redoStates: Map<string, ColorPixel>[];

  srcViewActive = true;
  imgViewActive = true;
  toolbarActive = true;
  fullscreen = false;
  cursor = 'pointer';
  selectedColor = {r: 97, g: 190, b: 188};
  image: Image;
  showColorPicker = false;
  colorPickerStyle: any;
  showOverlay = false;
  setColor = {r: 97, g: 190, b: 188};
  toolbarInfo = {isInit: false, format: -1};
  pickerStyle = 0;

  /**
   * Der Konstrukter nimmt die Injectables an, welche zur steuerung der Anwendung benötigt werden.
   * Dem konfigurations Service wird ein Callback zur Initialisierung des Bildes übergeben.
   * @param {SrcRenderControllerService} srcRenderer Renderer der ASCII-Ansicht
   * @param {ImgRenderControllerService} imgRenderer Renderer der Bild-Ansicht.
   * @param {FileManagerService} fileManager Service zum Speichern und Laden des Bildes.
   * @param {ConfigStorageService} config Ein Konfigurationsspeicher, der das JSON File zu Beginn einliest.
   */
  constructor(private srcRenderer: SrcRenderControllerService,
    private imgRenderer: ImgRenderControllerService,
    private fileManager: FileManagerService,
    private config: ConfigStorageService) {
      this.color = new ColorElement(97, 190, 188);
      this.config.init((img, color) => {
        this.image = img;
        this.isInit = true;
        this.toolbarInfo = {isInit: true, format: Number(img.format)};
        this.setColor = color;
        this.selectedColor = color;
      });

    }

  /**
   * Prüft, ob das Objekt wie gefordert geändert werden kann.
   * @param {ResizeEvent} event Die größe, umd die das Objekt verschoben werden muss.
   * @returns Die verbesserte Größe.
   */
  resize(event: ResizeEvent): ResizeEvent {
    const containerRight = this.container.offsetLeft + this.container.offsetWidth;
    const contentRight = this.content.offsetLeft + this.content.offsetWidth;
    if (contentRight + event.width < containerRight - 2) {
      event.setSize(event.width, event.height);
      return event;
    }
    event.setSize(containerRight - contentRight - 2, containerRight - contentRight - 2);
    return event;
  }

  /**
   * Setzt die mininmale Größe (Veraltet durch den konfigurations Service).
   */
  setMinSize(pMinSize: number) {
    this.minSize = pMinSize;
  }

  /**
   * Setzt den Container und den Inhalt, welche bei einer Größenänderung nicht kolidieren dürfen.
   */
  setCollisionObjects(pContainer: HTMLElement, pContent: HTMLElement) {
    this.container = pContainer;
    this.content = pContent;
  }

  /**
   * Es wurde Auf den Farb Button gedrückt. Öffnet den ColorPicker und setzt
   * in an das übergebene Element.
   * @param {EditorEvent} event enthält das Element, bei welchem der ColorPicker angezeigt werden soll.
   */
  eventColor(event: EditorEvent) {
    if (!this.isInit) {
      return;
    }
    this.pickerStyle = this.image.format;
    const target = <HTMLElement>event.payload.srcElement || <HTMLElement>event.payload.target;
    const x = target.getBoundingClientRect().left;
    const y = target.offsetHeight + target.getBoundingClientRect().top;
    this.colorPickerStyle = {'position': 'fixed', 'left': x + 'px', 'top': y + 'px'};
    // 'transform': 'translate(' +  x + 'px,' + y + 'px)'};
    this.showColorPicker = true;
  }

  /**
   * Das Format wird hier geändert. Kann durch verschiedene Ereignise ausgelöst werden.
   * @param {number} format enthält das Format. 0 = PPM, 1 = PGM, 2 = PBM.
   */
  eventNewFormat(format: number) {
    this.image.format = format;
    if (format === 1) {
      const gray = (this.selectedColor.r + this.selectedColor.g + this.selectedColor.b) / 3;
      this.selectedColor = {r: gray, g: gray, b: gray};
    }
    this.imgRenderer.drawGrid();
    this.srcRenderer.reConfig();
    this.srcRenderer.draw();
  }

  /**
   * Aktiviert das Overlay für ein neues Bild.
   */
  eventNew() {
    this.showOverlay = true;
  }

  /**
   * Stellt eine neue Farbe im Controller ein.
   */
  newColor(color: {r: number, g: number, b: number}) {
    this.color = new ColorElement(color.r, color.g, color.b);
    this.selectedColor = color;
  }

  /**
   * Stellt eine neue Farbe im Controller ein.
   */
  eventSave() {
    this.fileManager.saveImg(this.image);
  }

  /**
   * Lädt die übergebene File Klasse als Bild.
   */
  eventLoad(file: File) {
    const setImg = (img: Image): void => {
      this.image = img;
      this.isInit = true;
      this.toolbarInfo = {isInit: true, format: Number(img.format)};
    };
    this.fileManager.loadImg(file, setImg);
  }

  /**
   * Akt- oder Deakt-iviert die Bild-Ansicht über Angular Binding.
   */
  eventImgView(isActive: boolean) {
    this.imgViewActive = isActive;
  }

  /**
   * Akt- oder Deakt-iviert die ASCII-Ansicht über Angular Binding.
   */
  eventSrcView(isActive: boolean) {
    this.srcViewActive = isActive;
    if (this.srcViewActive) {
      this.srcRenderer.reConfig();
      this.srcRenderer.draw();
    }
  }

  /**
   * Setzt den Tool-mode auf Pinsel (normales Zeichnen).
   */
  eventPencil() {
    this.toolmode = 0;
    this.imgRenderer.setToolmode(0);
    this.cursor = 'pointer';
  }

  /**
   * Setzt den Tool-mode auf die Rechteckauswahl.
   */
  eventRectangle() {
    this.toolmode = 1;
    this.imgRenderer.setToolmode(1);
  }

  /**
   * Setzt den Tool-mode auf den Radiergummi.
   */
  eventRubber() {
    this.toolmode = 2;
    this.imgRenderer.setToolmode(2);
    this.cursor = 'pointer';
  }

  /**
   * Setzt den Tool-mode auf Pipette (Farbe aswählen).
   */
  eventPipette() {
    this.toolmode = 3;
    this.imgRenderer.setToolmode(3);
    this.cursor = 'crosshair';
  }

  /**
   * Akt- oder Deakt-iviert das Gitter in der Bild-Ansicht
   */
  eventGrid(isActive: boolean) {
    this.imgRenderer.gridOn = isActive;
    this.imgRenderer.drawGrid();
  }

  /**
   * Prüft, ob es Pixelwerte zum rückgängig machen muss.
   * Die Undo States verwalten in einzelnen Schritten die Veränderte Pixelwerte.
   */
  eventUndo() {
    if (this.undoStates === undefined) {
      return;
    }

    if (this.undoStates.length < 1) {
      return;
    }
    this.undo();
  }

  eventRedo() {
    if (this.redoStates === undefined) {
      return;
    }

    if (this.redoStates.length < 1) {
      return;
    }
    this.redo();
  }

  /**
   * Blendet andere Elemente aus, um die Bild-Ansicht in den Vollbildmodus zu setzen.
   */
  fullscreenImg() {
    this.fullscreen = true;
    this.toolbarActive = false;
    this.srcViewActive = false;
    this.imgSize = new ResizeEvent(700, 0 , false);
  }

  /**
   * Blendet andere Elemente aus, um die ASCII-Ansicht in den Vollbildmodus zu setzen.
   */
  fullscreenSrc() {
    this.fullscreen = true;
    this.toolbarActive = false;
    this.imgViewActive = false;
    this.srcSize = new ResizeEvent(700, 700 , false);
  }

  /**
   * Setzt die Änderungen um, welche in der Bildansicht aufgetreten sind. Die verschiedene Toolmodis werden dabei beachtet.
   * @param {ImgChangeEvent} event Enthält die Daten, an welcher Stelle was geändert wurde.
   */
  imgChange(event: ImgChangeEvent) {
    if (!(event instanceof ImgChangeEvent)) {
      return;
    }
    if (!this.image) {
      return;
    }
    if (this.toolmode === 0) {
      this.setUndo(event);
      this.image.pixelList[event.PosX][event.PosY] = this.color;
      this.srcRenderer.highlight(event.PosX, event.PosY);
    } else if (this.toolmode === 1) {
      this.setRect(event);
    } else if (this.toolmode === 2) {
      this.setUndo(event);
      this.image.pixelList[event.PosX][event.PosY] = new ColorElement(255, 255, 255);
      this.srcRenderer.highlight(event.PosX, event.PosY);
    } else if (this.toolmode === 3) {
      this.selectedColor = event.color;
      this.setColor = event.color;
      this.color = new ColorElement(event.color.r, event.color.g, event.color.b);
    }
    this.srcRenderer.draw();
  }

  /**
   * Setzt die Änderungen um, welche in der ASCII-Ansicht aufgetreten sind (Auch änderung des Formats oder des Namen).
   * @param {SrcChangeEvent} event Enthält die Daten, an welcher Stelle was geändert und wurde und die Eingabe.
   */
  srcChange(event: SrcChangeEvent) {
    if (!(event instanceof SrcChangeEvent)) {
      console.error('editor.component srcChange(): event ist not instance of SrcChangeEvent');
      return;
    }
    if (event.mode === 1) {
      this.eventNewFormat(event.payload);
      return;
    }
    if (event.mode === 2) {
      this.image.name = event.payload;
      this.srcRenderer.draw();
      return;
    }
    this.setUndo(event);
    this.image.pixelList[event.PosX][event.PosY] = new ColorElement(event.red, event.green, event.blue);
    this.imgRenderer.drawGrid();
    this.srcRenderer.draw();
  }

  /**
   * Setzt den UndoState. Bei einem EventState von 0, wird ein neuer Schritt erstellt. Bei einem anderen, wird die änderung des Pixels
   * an den voherigen gehängt.
   * @param {any} event Abhängig von der Änderung, wird der Aktuelle Pixelwert gespeichert.
   */
  setUndo(event: any) {
    if (this.undoStates === undefined || this.undoStates.length <= 0 || event.state === 0) {
      const colorMap = new Map<string, ColorPixel>();
      const colorPixel = new ColorPixel(event.PosX, event.PosY, this.image.pixelList[event.PosX][event.PosY]);
      colorMap.set(event.PosX + ':' + event.PosY, colorPixel);
      if (this.undoStates === undefined) {
        this.undoStates = new Array(colorMap);
      } else {
        this.undoStates.push(colorMap);
      }
    } else {
      const index = this.undoStates.length - 1;
      const colorPixel = new ColorPixel(event.PosX, event.PosY, this.image.pixelList[event.PosX][event.PosY]);
      if  (!this.undoStates[index].has(event.PosX + ':' + event.PosY)) {
        this.undoStates[index].set(event.PosX + ':' + event.PosY, colorPixel);
      }
    }
    delete this.redoStates;
  }

  /**
   * Ändert Mehrere Pixel in einem Rechteck welche bei der Rechteckauswahlt selektiert wurden.
   * @param {ImgChangeEvent} event Enthält die Koordinaten, Länge und Breite.
   */
  setRect(event: ImgChangeEvent) {
    if (event.state !== 0) {
      this.undo();
    }

    const colorMap = new Map<string, ColorPixel>();
    delete this.redoStates;

    for (let i = 0; i < event.width; i++) {
      for (let j = 0; j < event.height; j++) {
        const x = event.PosX + i;
        const y = event.PosY + j;
        const colorPixel = new ColorPixel(x, y, this.image.pixelList[x][y]);
        this.srcRenderer.highlight(x, y);
        colorMap.set(x + ':' + y, colorPixel);
        this.image.pixelList[x][y] = this.color;
      }
    }
    if (this.undoStates === undefined) {
      this.undoStates = new Array(colorMap);
    } else {
      this.undoStates.push(colorMap);
    }
  }

  /**
   * Macht den letzten Schritt in den Undo States Rückgängig.
   * Die Undo States verwalten in einzelnen Schritten die Veränderte Pixelwerte.
   * Die Rückgängig gemachten Schritte werden in den Redo States gespeichert.
   */
  private undo() {

    const redoMap = new Map<string, ColorPixel>();
    const undoMap = this.undoStates.pop();

    undoMap.forEach(element => {
      const colorPixel = new ColorPixel(element.x, element.y, this.image.pixelList[element.x][element.y]);
      redoMap.set(element.x + ':' + element.y, colorPixel);
      this.image.pixelList[element.x][element.y] = element.color;
    });

    if (this.redoStates === undefined) {
      this.redoStates = new Array(redoMap);
    } else {
      this.redoStates.push(redoMap);
    }

    this.imgRenderer.drawGrid();
    this.srcRenderer.draw();
  }

  /**
   * Macht den letzten Schritt in den Redo States Rückgängig.
   */
  private redo() {

    const undoMap = new Map<string, ColorPixel>();
    const redoMap = this.redoStates.pop();

    redoMap.forEach(element => {
      const colorPixel = new ColorPixel(element.x, element.y, this.image.pixelList[element.x][element.y]);
      undoMap.set(element.x + ':' + element.y, colorPixel);
      this.image.pixelList[element.x][element.y] = element.color;
    });

    if (this.undoStates === undefined) {
      this.undoStates = new Array(undoMap);
    } else {
      this.undoStates.push(undoMap);
    }

    this.imgRenderer.drawGrid();
    this.srcRenderer.draw();
  }

  /**
   * Erstellt ein neues Bild.
   * @param {ImgChangeEvent} event Enthält die Daten des neuen Bildes.
   */
  createNew(event: NewEvent) {
    this.showOverlay = false;
    if (!event.valid) {
      return;
    }
    this.isInit = true;
    const image = new Image(event.height, event.width);
    image.format = Number(event.format);
    image.name = event.name;
    this.image = image;
    this.toolbarInfo = {isInit: true, format: Number(event.format)};
  }

  /**
   * Die Funktion wird aufgerufen, wenn der ColorPicker geschlossen wird.
   */
  colorPickerClose(color: {r: number, g: number, b: number}) {
    if (color === undefined) {
      return;
    }
    this.setColor = color;
    this.showColorPicker = false;
  }

  /**
   * Wenn der Vollbildmodus beendet wird, werden die Ansichten eingeschalten und in die Grundgröße versetzt.
   */
  endFullscreen() {
    if (this.fullscreen) {
      this.toolbarActive = true;
      this.imgViewActive = true;
      this.srcViewActive = true;
      this.imgSize = new ResizeEvent(500, 500, false);
      this.srcSize = new ResizeEvent(500, 500, false);
      this.imgRenderer.drawGrid();
      this.srcRenderer.reConfig();
      this.srcRenderer.draw();
    }
  }
}
