import { TestBed, inject } from '@angular/core/testing';

import { ImgRenderControllerService } from './img-render-controller.service';

describe('ImgRenderControllerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ImgRenderControllerService]
    });
  });

  it('should be created', inject([ImgRenderControllerService], (service: ImgRenderControllerService) => {
    expect(service).toBeTruthy();
  }));
});
