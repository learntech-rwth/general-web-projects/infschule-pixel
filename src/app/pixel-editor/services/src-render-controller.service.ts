import { Injectable } from '@angular/core';
import { Image } from '../shared/image';
import { ResizeEvent } from '../shared/events/resize-event';
import { SrcChangeEvent } from '../shared/events/src-change-event';
import { format } from 'url';
import { ConfigStorageService } from '../services/config-storage.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { colorInputValidator, blackInputValidator, maxSizeValidator } from '../shared/max-size-validator';
import { ValidatorFn } from '@angular/forms/src/directives/validators';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/takeWhile';

interface RectPos {
  width: number;
  height: number;
}

interface Color {
  r: number;
  g: number;
  b: number;
}

/**
 * Dies ist der Render-Service für die ASCII-Ansicht. Er speichert alle ausgerechnete Werte für
 * das Renrn lokal.
 */
@Injectable()
export class SrcRenderControllerService {

  canvas: HTMLCanvasElement;
  isInit = false;
  img: Image;
  lastColor = {r: 255, g: 255, b: 255};

  imgDataRows = 4; // Datenfelder. 4 Bei PPM und PGM, 3 bei PBM.
  boxesPerRow: number; // Wie viele Farbwerte stehen in einer Reihe. (R G B) zählt hier auch als einen Wert.
  textPos: RectPos; // Die Position, des angeklickten Wertes in der Ansicht und nicht im Bild selber.
  paddingWidth: number; // Wie viel Platz zwischen den Werten ist.
  paddingHeight: number; // Wie viel Platz unter und über den Werten ist.
  boxHeight: number; // Die "Höhe" oder auch Schriftgröße eines Wertes.
  boxWidth: number; // Die "Weite" bzw. Schriftlänge eines Wertes
  textHeight: number;

  // config. Können in der Konfiguration eingestellt werden.
  minHeightPadding = 0;
  maxTextHeight = 0;
  minTextHeight = 0;
  sidePadding = 0;
  srcHeight = 500;
  srcWidth = 500;
  minViewWidth = 100;
  minViewHeight = 100;
  HWfactorPBM = 2 / 3; // Todo
  HWfactorPGM = 2; // Todo
  HWfactorPPM = 6; // Todo

  // hilight. Für die Hervorhebung der Werte.
  colorSteps = {r: 0, g: 0, b: 0};
  highlightColor = {r: 0, g: 255, b: 255};
  highlightTime = 5000;
  highlightInterval = 500;
  highlightOn = false;
  hightlights = new Map<number, {c: Color, step: number}>();

  /**
   * Der Konstruktor nimmt die Konfiguration entgegen
   * @param {ConfigStorageService} config Enthält die im JSON File konfigurierte Werte
   */
  constructor(private config: ConfigStorageService) {
    this.textPos = {width: 0, height: 0};

  }

  /**
   * Der Controller muss für die meisten Funktionen erst Initialisiert werden.
   * @param {HTMLCanvasElement} canvas Das Canvas Objekt, auf welches sich die Funktionen Beziehen.
   * @param {Image} img Die Bild Daten, welche gezeichnet werden.
   * @returns Die änderung der Canvas größe.
   */
  init(canvas: HTMLCanvasElement, img: Image): ResizeEvent {
    this.canvas = canvas;

    this.img = img;
    this.isInit = true;
    this.minHeightPadding = this.config.srcView.minHeightPadding;
    this.maxTextHeight = this.config.srcView.maxTextHeight;
    this.minTextHeight = this.config.srcView.minTextHeight;
    this.sidePadding = this.config.srcView.sidePadding;
    this.HWfactorPBM = this.config.srcView.HWfactorPBM;
    this.HWfactorPGM = this.config.srcView.HWfactorPGM;
    this.HWfactorPPM = this.config.srcView.HWfactorPPM;
    this.srcWidth = this.config.srcView.initialWidth;
    this.srcHeight = this.config.srcView.initialHeight;
    this.minViewWidth = this.config.srcView.minViewWidth;
    this.minViewHeight = this.config.srcView.minViewHeight;
    this.highlightColor = this.config.srcView.highlightColor;
    this.highlightTime = this.config.srcView.highlightTime;
    this.highlightInterval = this.config.srcView.highlightInterval;
    const steps = this.highlightTime / this.highlightInterval;
    this.colorSteps.r = Math.floor((255 - this.highlightColor.r) / steps);
    this.colorSteps.g = Math.floor((255 - this.highlightColor.g) / steps);
    this.colorSteps.b = Math.floor((255 - this.highlightColor.b) / steps);
    this.reConfig();
    return new ResizeEvent(0, 0);
  }

  /**
   * Fügt ein Highlight-Element dem Klassenmember highlights hinzu. Über einen Timer, werden in dem eingestellten Intervall
   * alle hilights neu gerendert. Falls keine mehr exisistieren, zerstört sich der Timmer um nicht immer die ASCCI-Ansicht
   * neu zu Rendern. Falls kein Highlight existiert, wird ein neuer Timer erstellt.
   * @param {number} x Die X Position im Bild.
   * @param {number} y Die Y Position im Bild.
   */
  highlight(x: number, y: number) {
    const index = y * this.img.getwidth() + x;
    const hColor = {r: this.highlightColor.r, g: this.highlightColor.g, b: this.highlightColor.b};
    this.hightlights.set(index, {c: hColor, step: 0});
    if (this.highlightOn) {
      return;
    }
    this.highlightOn = true;
    const steps = this.highlightTime / this.highlightInterval;
    const timer = Observable.timer(0, this.highlightInterval);
    timer.takeWhile(t => {
      if (this.hightlights.size > 0) {
        return true;
      } else {
        this.highlightOn = false;
        return false;
      }
    }).subscribe(t => {
      this.hightlights.forEach((value, key) => {
        if (value.step === steps) {
          this.hightlights.delete(key);
          return;
        }
        value.c.r += this.colorSteps.r;
        value.c.g += this.colorSteps.g;
        value.c.b += this.colorSteps.b;
        value.step++;
      });
      this.draw();
    });

  }

  /**
   * Wird aufgerufen, wenn sich die Canvasgröße ändert. Die Werte, wie groß die Schrift ist und
   * wie viele Farbwerte in eine Zeile passen werden gespeichert um diese nicht jedesmal neu zu berechnen.
   */
  reConfig(): number {
    if (!this.isInit) {
      return 0;
    }
    if (this.img.format === 2) {
      this.imgDataRows = 3;
    } else {
      this.imgDataRows = 4;
    }
    const ctx = this.canvas.getContext('2d');
    this.canvas.width = this.srcWidth;
    this.canvas.height = this.srcHeight;

    // Breite des Textes, abhängig von der Höhe
    let HWfactor = this.HWfactorPBM;
    if (this.img.format === 0) {
      HWfactor = this.HWfactorPPM;
    } else if (this.img.format === 1) {
      HWfactor = this.HWfactorPGM;
    }

    // Minimale Breite, ausgerechnet von der angegebenen minimalen Höhe des Textes.
    const minWidth =  HWfactor * this.minTextHeight;
    const res = this.canvas.width - (this.img.getwidth() * (minWidth + this.sidePadding)) - this.sidePadding;
    this.paddingWidth = this.sidePadding;

    if (res < 0) { // Wenn bei den minimalen Einstellung kein Platz für alle Werte in einer Reihe sind.
      this.boxesPerRow = Math.floor((this.canvas.width - this.sidePadding) / (minWidth + this.sidePadding));
      this.boxesPerRow = Math.max(this.boxesPerRow, 1);
      this.boxWidth = minWidth;
      this.boxHeight = minWidth / HWfactor;
    } else { // Es ist Platz, um die Schrift und/oder das Padding zu vergrößern.
      this.boxesPerRow = this.img.getwidth();
      const newWidth = minWidth + (res / this.img.getwidth());
      const newHeight = newWidth / HWfactor;
      if (newHeight > this.maxTextHeight) {
        this.boxWidth = this.maxTextHeight * HWfactor;
        this.boxHeight = this.maxTextHeight;
        this.paddingWidth += newWidth - this.boxWidth;
      } else {
        this.boxWidth = newWidth;
        this.boxHeight = newHeight;
      }
    }
    // Wie viele Zeilen für die Darstellung einer Pixelreihe im Bild notwendig sind.
    const rowsPerLine = Math.ceil(this.img.getwidth() / this.boxesPerRow);

    const height = (rowsPerLine * this.img.getheight() + this.imgDataRows) * this.boxHeight;
    this.paddingHeight = (this.canvas.height - height) / ((this.img.getheight() * rowsPerLine) + this.imgDataRows);
    this.paddingHeight = Math.max(this.paddingHeight, this.minHeightPadding);
    return 0;
  }

  /**
   * Zeichnet alle Highlight-Elemente
   */
  drawHilight() {
    if (!this.isInit) {
      return;
    }
    if (this.hightlights.size === 0) {
      return;
    }

    const ctx = this.canvas.getContext('2d');
    ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    const width = this.img.getwidth();

    const rowsPerLine = Math.ceil(this.img.getwidth() / this.boxesPerRow);
    const offsetHeight = this.boxHeight + this.paddingHeight;
    const offsetWidth = this.boxWidth + this.paddingWidth;

    this.hightlights.forEach((value, key) => {
      const y = Math.floor(key / width);
      let x = key - (y * width);
      const addLines = Math.floor(x / this.boxesPerRow);
      x = x - (addLines * this.boxesPerRow);
      ctx.beginPath();
      ctx.fillStyle = 'rgb(' + value.c.r + ',' + value.c.g + ',' + value.c.b + ')';
      ctx.rect((x * offsetWidth), offsetHeight * ((y * rowsPerLine) + addLines + this.imgDataRows), offsetWidth, offsetHeight);
      ctx.fill();
    });
  }

  /**
   * Zeichnet anhand der in der reConfig() ausgerechneten Daten, die ASCII-Ansicht.
   */
  draw() {
    if (!this.isInit) {
      return;
    }
    const ctx = this.canvas.getContext('2d');
    ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    const offsetHeight = this.boxHeight + this.paddingHeight;
    const dataOffset = this.imgDataRows * offsetHeight;
    const rowsPerLine = Math.ceil(this.img.getwidth() / this.boxesPerRow);
    this.drawHilight();
    ctx.fillStyle = 'black';
    ctx.textBaseline = 'hanging';
    ctx.font =  this.boxHeight + 'pt Arial';
    ctx.fillText('#' + this.img.name, this.paddingWidth / 2, this.paddingHeight / 2);
    ctx.fillText(this.getFormatText(), this.paddingWidth / 2, offsetHeight + this.paddingHeight / 2);
    ctx.fillText(this.getResText(), this.paddingWidth / 2, (offsetHeight * 2) + this.paddingHeight / 2);
    if (this.img.format !== 2) {
      ctx.fillText('255', this.paddingWidth / 2, (offsetHeight * 3) + this.paddingHeight / 2);
    }
    for (let i = 0; i < this.img.getheight(); i++) {
      let boxesLeft = this.img.getwidth();
      for (let j = 0; j < rowsPerLine; j++) {
        const condition = Math.min(boxesLeft, this.boxesPerRow);
        for (let k = 0; k < condition; k++) {
          let text: string;
          if (this.img.format === 0) {
            text = this.img.pixelList[k + j * this.boxesPerRow][i].getColorText();
          } else if (this.img.format === 1) {
            text = this.img.pixelList[k + j * this.boxesPerRow][i].getGrayText();
          } else {
            text = this.img.pixelList[k + j * this.boxesPerRow][i].getBlackText();
          }
          ctx.textBaseline = 'hanging';
          ctx.font =  this.boxHeight + 'pt Arial';
          const width = k * (this.boxWidth + this.paddingWidth) + this.paddingWidth / 2;
          const height = dataOffset + ((i * rowsPerLine) + j) * offsetHeight + this.paddingHeight / 2;
          ctx.fillText(text, width, height, this.boxWidth);
        }
        boxesLeft -= this.boxesPerRow;
      }
    }
  }

  private drawColor() {

  }

  /**
   * Gibt die Pixel-Position zurück, an der das Eingabefeldgeöffnet wurde.
   * Die Farbe wird gespeichert.
   * @returns Die Position wird zusammen mit der übergebenen Farbe in einem Event gespeichert.
   */
  getChangeEvent(r: number, g: number, b: number): SrcChangeEvent {
    this.lastColor = {r: r, g: g, b: b};
    return new SrcChangeEvent(this.textPos.width, this.textPos.height - this.imgDataRows, r, g, b);
  }

  /**
   * Gibt die Pixel-Position zurück, an der das Eingabefeldgeöffnet wurde.
   * @returns Die Position wird zusammen mit der letzten Farbe in einem Event gespeichert.
   */
  getChangeEventVoid() {
    const r = this.lastColor.r;
    const g = this.lastColor.g;
    const b = this.lastColor.b;
    return new SrcChangeEvent(this.textPos.width, this.textPos.height - this.imgDataRows, r, g, b);
  }

  /**
   * Prüft das Eingabefeld für 1 Wort. Das Feld für PBM, PGM und Bilddaten benutzt.
   * Für RGB Farben
   * Die Eingabe wird ausgewertet.
   * @returns Das Entsprechende Event wird zurückgegeben
   */
  checkInput(event: any): SrcChangeEvent {
    if (!this.isInit) {
      return null;
    }

    if (!event.target.value) {
      return null;
    }

    // Dateneingabe
    if (this.textPos.height < this.imgDataRows) {
      if (this.textPos.height === 1) {
        const srcChange = new SrcChangeEvent();
        srcChange.mode = 1;
        if (String(event.target.value) === 'P1') {
          srcChange.payload = 2;
        } else if (String(event.target.value) === 'P2') {
          srcChange.payload = 1;
        } else {
          srcChange.payload = 0;
        }
        return srcChange;
      }
      if (this.textPos.height === 0) {
        const srcChange = new SrcChangeEvent();
        srcChange.mode = 2;
        srcChange.payload = String(event.target.value).substr(1);
        return srcChange;
      }
    }

    // Farbwerteingabe
    if (this.img.format === 2) {
      const value = Number(event.target.value);
      return this.checkInputBlack(value);
    } else if (this.img.format === 1) {
      const value = Number(event.target.value);
      return this.checkInputGray(value);
    }
  }


  checkInputBlack(value: number): SrcChangeEvent {
    if (value === 0) {
      this.lastColor = {r: 255, g: 255, b: 255};
      return new SrcChangeEvent(this.textPos.width, this.textPos.height - this.imgDataRows, 255, 255, 255);
    } else if (value === 1) {
      this.lastColor = {r: 0, g: 0, b: 0};
      return new SrcChangeEvent(this.textPos.width, this.textPos.height - this.imgDataRows, 0, 0, 0);
    } else {
      return null;
    }
  }

  checkInputGray(value: number): SrcChangeEvent {
    if (value < 0 || value > 255) {
      return null;
    }
    this.lastColor = {r: value, g: value, b: value};
    return new SrcChangeEvent(this.textPos.width, this.textPos.height - this.imgDataRows, value, value, value);
  }

  /**
   * Gibt den entsprechenden CSS Style für das Eingabefeld an Pos X und Y zurück.
   */
  getColorInput(textPosX, textPosY): any {
    return {'left': (textPosX - (1 / 2) * this.boxWidth) + 'px',
    'top': textPosY - 10 + 'px',
    'width': this.boxWidth + 85 + 'px',
    'height': this.boxHeight + 17 + 'px',
    'font-size': this.textHeight + 'px',
    'padding-top': (this.boxHeight + 4 - this.minTextHeight) / 2 + 'px'
   };
  }

  /**
   * Gibt den entsprechenden CSS Style für das Eingabefeld an Pos X und Y zurück.
   */
  getGrayInput(textPosX, textPosY): any {
    return {'left': (textPosX - 10) + 'px',
    'top': textPosY - 5 + 'px',
    'width': this.boxWidth + 10 + 'px',
    'height': this.boxHeight + 10 + 'px',
    'font-size': this.textHeight + 'px'
    };
  }

  /**
   * Gibt den entsprechenden CSS Style für das Eingabefeld an Pos X und Y zurück.
   */
  getBlackInput(textPosX, textPosY): any {
    return {'left': (textPosX - 10) + 'px',
    'top': textPosY - 5 + 'px',
    'width': this.boxWidth + 10 + 'px',
    'height': this.boxHeight + 10 + 'px',
    'font-size': this.textHeight + 'px'
    };
  }

  /**
   * Gibt den entsprechenden CSS Style für das Eingabefeld an Pos X und Y zurück.
   */
  getDataInput(textPosX, textPosY): any {
    return {'left': textPosX + 'px',
    'top': textPosY - 5 + 'px',
    'width': this.canvas.width - 5 + 'px',
    'height': this.boxHeight + 10 + 'px',
    'font-size': this.textHeight + 'px'
    };
  }

  /**
   * Gibt das aktuelle Format als String zurück.
   */
  getFormatText(): string {
    if (this.img.format === 0) {
      return 'P3';
    } else if (this.img.format === 1) {
      return 'P2';
    } else if (this.img.format === 2) {
      return 'P1';
    }
    return '#';
  }

  /**
   * Berechnet bei einem Klick den ausgewählten Pixel-Position im Bild (Die Farbwerte für eine Pixlereihe im Bild können in
   * unterschieldichen Zeilen der ASCII-Ansicht sein). Dieser Wert wird gespeichert (in textPos). Von diesen Daten aus, wird die
   * Position des Inputfeldes berechnet.
   * @param {MouseEvent} event Das MouseEvent, welches die Position enthält wo in der Ansicht geklickt wurde.
   * @returns Null, falls die Position außerlhabl ist, sonst die ausgerechnete Position im HTML-Dokument
   */
  setInputPixelFocus(event: MouseEvent): any {
    if (!this.isInit) {
      return null;
    }
    const rowsPerLine = Math.ceil(this.img.getwidth() / this.boxesPerRow);
    const offsetHeight = this.boxHeight + this.paddingHeight;
    const dataOffset = this.imgDataRows * offsetHeight;
    const offsetX =  event.clientX - this.canvas.getBoundingClientRect().left;
    const offsetY = event.clientY - this.canvas.getBoundingClientRect().top - dataOffset;
    let x = Math.floor((offsetX + 1) / (this.boxWidth + this.paddingWidth));
    const y = Math.floor((offsetY + 1) / offsetHeight);
    let line = Math.floor(y / rowsPerLine);
    const lineOffset = y - (line * rowsPerLine);

    if (offsetX > this.canvas.width || offsetX < 0 || offsetY + dataOffset > this.canvas.height) {
      return null;
    }

    if (line >= this.img.getwidth() || y >= this.img.getheight() * rowsPerLine) {
      return null;
    }

    if (offsetY < 0) {
      if (offsetY + dataOffset < 0) {
        return null;
      }
      x = 0;
      line = Math.floor((offsetY + dataOffset) / offsetHeight);
    } else {
      line += this.imgDataRows;
    }

    if (lineOffset * this.boxesPerRow + x >= this.img.getwidth()) {
      return null;
    }

    this.textPos = {width: (lineOffset * this.boxesPerRow + x), height: line};
    const textPosX = x * (this.boxWidth + this.paddingWidth) + 0.5 * this.paddingWidth;
    const textPosY = y * offsetHeight + 0.5 * this.paddingHeight + dataOffset;
    return {x: textPosX, y: textPosY};
  }

  /**
   * Ruft setInputPixelFocus(MouseEvent) auf. Abhängig von den der geklickten Zeile und des Formates, werden
   * die passenden Styles für das Eingabefeld geladen.
   * Zeile 1-4 Können daten Zeilen sein und die Restlichen Styles, sind Abhängig vom Format.
   * @param {MouseEvent} event Das MouseEvent, welches die Position enthält wo in der Ansicht geklickt wurde.
   * @returns Null, falls die Position außerhalb ist, sonst den passenden Style.
   */
  drawInpuBox(event: MouseEvent): any {
    if (!this.isInit) {
      return null;
    }

    const pos = this.setInputPixelFocus(event);
    if (!pos) {
      return null;
    }

    const textPosX = pos.x;
    const textPosY = pos.y;

    if (this.textPos.height < this.imgDataRows) {
      if (this.textPos.height === 2 || this.textPos.height === 3) {
        return null;
      }
      return this.getDataInput(0, textPosY);
    } else if (this.img.format === 0) {
      return this.getColorInput(textPosX, textPosY);
    } else if (this.img.format === 1) {
      return this.getGrayInput(textPosX, textPosY);
    } else if (this.img.format === 2) {
      return this.getBlackInput(textPosX, textPosY);
    }

    return null;
  }

  /**
   * Getter.
   * @returns Das Format.
   */
  getFormat(): number {
    return this.img.format;
  }

  /**
   * Gibt den Text für das Datenfeld der Auflösung zurück.
   * @returns Das Format.
   */
  getResText(): string {
    return String(this.img.getwidth()) + ' ' + String(this.img.getheight());
  }

  /**
   * Prüft ob das erweiterte Eingabefeld für RGB eingeblendet werden muss.
   * @returns Ob es sich um eine RGB Eingabe handlet.
   */
  isColorFormat(): boolean {
    if (this.getFormat() === 0) {
      if (this.textPos.height >= this.imgDataRows) {
        return true;
      }
    }
    return false;
  }

  /**
   * Generiert die FormGroup für das Formular mit den passenden Validatoren für die Eingabe.
   * @returns Die FormGroup
   */
  getFormGroup(): FormGroup {
    if (this.textPos.height < this.imgDataRows) {
      if (this.textPos.height === 0) {
        return this.generateFormGroup(Validators.required,  Validators.pattern('#[^\\\\|\/|\:|\*|\?|\"|\<|\>|\|]+'));
      } else if (this.textPos.height === 1) {
        return this.generateFormGroup(Validators.required,   Validators.pattern('P1|P2|P3'));
      } else if (this.textPos.height === 2) {
        return this.generateFormGroup(Validators.required,  colorInputValidator(255));
      } else if (this.textPos.height === 3) {
        return this.generateFormGroup(Validators.required,  maxSizeValidator(255));
      }
    }
    if (this.getFormat() === 0) {
      return this.generateFormGroup(Validators.required,  colorInputValidator(255));
    } else if (this.getFormat() === 1) {
      return this.generateFormGroup(Validators.required,  colorInputValidator(255));
    } else if (this.getFormat() === 2) {
      return this.generateFormGroup(Validators.required,  blackInputValidator());
    }
    return null;
  }

  /**
  * Die Vorlage für die FormGroup, da alle genau 2 Validatoren benutzen (Required ist immer enthalten).
  * @returns Die FormGroup in denen die Validatoren eingesetzt wurden.
  */
  generateFormGroup(validator1: ValidatorFn, validator2: ValidatorFn) {
    return new FormGroup({
      'input': new FormControl('', [
        validator1,
        validator2
      ]),
    });
  }

  /*
  getValidatorString(): string {
    if (this.img.format === 0) {
      return 'P1';
    } if (this.img.format === 1) {
      return 'P2';
    } if (this.img.format === 2) {
      return 'P3';
    }
  }*/
}
