import { TestBed, inject } from '@angular/core/testing';

import { SrcRenderControllerService } from './src-render-controller.service';

describe('SrcRenderControllerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SrcRenderControllerService]
    });
  });

  it('should be created', inject([SrcRenderControllerService], (service: SrcRenderControllerService) => {
    expect(service).toBeTruthy();
  }));
});
