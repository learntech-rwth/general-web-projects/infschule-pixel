import { Injectable } from '@angular/core';
import { Image } from '../shared/image';

/**
 * Dieser Service Lädt oder Speichert ein Bild
 */
@Injectable()
export class FileManagerService {

  // constructor() { }

  /**
   * Speichert ein Bild.
   * @param {Image} img Bild, welches gespeichert werden soll.
   */
  saveImg(img: Image) {
    const data = this.ImgToText(img);
    const blob = new Blob([data], {type: 'text/plain'});
    let name: string;

    if (img.format === 0) {
      name = 'Pixelbild.PPM';
    } else if (img.format === 1) {
      name = 'Pixelbild.PGM';
    } else if (img.format === 2) {
      name = 'Pixelbild.PBM';
    }

    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob, name);
    } else {
    const e = document.createEvent('MouseEvents');
    const a = document.createElement('a');

    a.download = name;
    a.href = window.URL.createObjectURL(blob);
    a.dataset.downloadurl = ['text/plain', a.download, a.href].join(':');
    e.initEvent('click', true, false);
    a.dispatchEvent(e);
    }
  }

  /**
   * Lädt ein Bild. Über ein Callback, wird das Bild übergeben, sobald die Datei
   * geladen und ausgewertet wurde.
   * @param {File} file Die Datei, welche geladen werden soll.
   * @param {(Image) => void} callback Übergibt das Bild diesem Callback, sobald es geladen wurde
   */
  loadImg(file: File, callback: (img: Image) => void) {
    const reader = new FileReader;
    const fileManager = this;
    reader.onload = function(e) {
      const newImg = fileManager.TextToImg(reader.result);
      newImg.name = file.name.slice(0, -4);
      if (newImg != null) {
        callback(newImg);
      }
    };
    reader.readAsText(file);
  }

  /**
   * Wandelt ein Bild in einen Text um, welcher gespeichert wird.
   * @param {Image} img Die Bilddatei, wie sie intern gespeichert wird.
   * @returns Ein String, in dem Offizielem Format des Bildes.
   */
  private ImgToText(img: Image): string {
    let fileText: string;
    fileText = this.getConfigText(img);
    for (let i = 0; i < img.getheight(); i++) {
      for (let j = 0; j < img.getwidth(); j++) {
        if (img.format === 0) {
          fileText += img.pixelList[j][i].getColorText();
        } else if (img.format === 1) {
          fileText += img.pixelList[j][i].getGrayText();
        } else if (img.format === 2) {
          fileText += img.pixelList[j][i].getBlackText();
        }
        fileText += '  ';
      }
      fileText += '\n';
    }
    return fileText;
  }

  /**
   * Wandelt einen gelesenen Text in ein Bild um. Zuerst werden alle Kommentare raus
   * genommen um das Bild weiter zu Parsen.
   * @param {string} fileText Der Text, wie er von der Datei eingelesen wurde.
   * @returns Die Bilddatei, wie sie intern gespeichert wird.
   */
  TextToImg(fileText: string): Image {
    let pos: number;
    while ((pos = fileText.indexOf('#')) !== -1) {
      const endPos = this.getBreakPos([fileText], pos);
      if (pos === 0) {
        fileText = fileText.slice(endPos);
      } else {
        fileText = fileText.slice(0, pos - 1) + fileText.slice(endPos);
      }
    }

    const textArray = fileText.split(/(\s+)/).filter( function(e) { return e.trim().length > 0; } );
    if (textArray.length < 3) {
      return null;
    }
    const width = Number(textArray[1]);
    const height = Number(textArray[2]);

    const img = new Image(height, width);
    if (String(textArray[0]) === 'P1') {
     this.getBlackImg(img, textArray);
    } else if (String(textArray[0]) === 'P2') {
      this.getGrayImg(img, textArray);
    } else if (String(textArray[0]) === 'P3') {
      this.getColorImg(img, textArray);
    }
    return img;
  }

  /**
   * In dieser Funktione wird die letzte Position einen Zeilenumbruchs bestimmt.
   * Es gibt 2 mögliche Zeilenumbrüche '\n' und '\r'.
   * @param {string[]} text Der String, als Array übergeben (So wird er al Referenz übergeben).
   * @param {number} start die Start Position, wo gesucht werden muss.
   * @returns Die letzte Position eines Zeilenumbruchs
   */
  private getBreakPos(text: string[], start: number): number {
    if (text.length === 0) {
      return -1;
    }
    const pos1 = text[0].indexOf('\n', start);
    const pos2 = text[0].indexOf('\r', start);
    if (pos1 < pos2) {
      if (pos1 !== -1) {
        return pos1;
      }
      return pos2;
    } else if (pos2 < pos1) {
      if (pos2 !== -1) {
        return pos2;
      }
      return pos1;
    }
    return -1;
  }

  /**
   * Die Daten werden im öffentlichen Format als Text zurückgegeben.
   * @param {Image} img Die Bilddatei, wie sie intern gespeichert wird.
   * @returns Die Daten als Text.
   */
  private getConfigText(img: Image): string {
    let configText: string;
    if (img.format === 0) {
      configText = 'P3 \n' + img.getwidth() + ' ' + img.getheight() + '\n255\n';
    } else if (img.format === 1) {
      configText = 'P2 \n' + img.getwidth() + ' ' + img.getheight() + '\n255\n';
    } else if (img.format === 2) {
      configText = 'P1 \n' + img.getwidth() + ' ' + img.getheight() + '\n';
    }
    return configText;
  }

  /**
   * Die schwarzen Werte aus dem Text, werden in den Pixeln gespeichert.
   * @param {Image} img Die Bilddatei, wie sie intern gespeichert wird. Die Pixel werden hier gespeichert.
   * @param {string[]} textArray Der eingelesene Text, gesplitet nach jedem Leerzeichen.
   */
  private getBlackImg(img: Image, textArray: string[]) {
    const width = img.getwidth();
    const height = img.getheight();
    if (textArray.length < 3 + width * height) {
      return;
    }
    img.format = 2;
    for (let i = 0; i < height; i++) {
      for (let j = 0; j < width; j++) {
        img.pixelList[j][i].setBlack(textArray[3 + i * width + j] === '1');
      }
    }
  }

  /**
   * Die grauen Werte aus dem Text, werden in den Pixeln gespeichert.
   * @param {Image} img Die Bilddatei, wie sie intern gespeichert wird. Die Pixel werden hier gespeichert.
   * @param {string[]} textArray Der eingelesene Text, gesplitet nach jedem Leerzeichen.
   */
  private getGrayImg(img: Image, textArray: string[]) {
    const width = img.getwidth();
    const height = img.getheight();
    if (textArray.length < 4 + width * height) {
      return;
    }
    img.format = 1;
    for (let i = 0; i < height; i++) {
      for (let j = 0; j < width; j++) {
        img.pixelList[j][i].setGray(Number(textArray[4 + i * width + j]));
      }
    }
  }

  /**
   * Die farbigen Werte aus dem Text, werden in den Pixeln gespeichert.
   * @param {Image} img Die Bilddatei, wie sie intern gespeichert wird. Die Pixel werden hier gespeichert.
   * @param {string[]} textArray Der eingelesene Text, gesplitet nach jedem Leerzeichen.
   */
  private getColorImg(img: Image, textArray: string[]) {
    const width = img.getwidth();
    const height = img.getheight();
    if (textArray.length < 4 + width * height * 3) {
      return;
    }
    img.format = 0;
    for (let i = 0; i < height; i++) {
      for (let j = 0; j < width * 3; j = j + 3) {
        const r = Number(textArray[4 + i * width * 3 + j]);
        const g = Number(textArray[4 + i * width * 3 + j + 1]);
        const b = Number(textArray[4 + i * width * 3 + j + 2]);
        img.pixelList[j / 3][i].setColor(r, g, b);
      }
    }
  }
}
