import { Injectable } from '@angular/core';
import { Image } from '../shared/image';
import {Http} from '@angular/http';

@Injectable()
/**
 * Der Konfigurationsspeicher, liest ein einen Pfad, in der URL aus "?config=Pfad" und
 * speichert die Einstellung in dieser Klasse.
 */
export class ConfigStorageService {

  data;
  isInit = false;
  toolbar = {
    showNew: true,
    showFullScreen: true,
    showFormat: true,
    showSave: true,
    showLoad: true,
    showColorPicker: true,
    showAsciiEnable: true,
    showImageEnable: true,
    showGridEnable: true,
    showUndo: true,
    showRedo: true,
    showTools: true,
    toggleColor: 'rgb(89, 216, 255)'};

  overlay = {
    maxWidth: 20,
    maxHeight: 20,
    initialName: 'Bild',
    initialHeight: 10,
    initialWidth: 10};

  imgView = {
    minViewWidth: 100,
    initialHeight: 500,
    initialWidth: 500,
    fullscreenWidth: 700,
    gridOn: true,
    gridStyle: '#d9d9d9',
    lineWidthFactor: 0.05,
    minLineWidth: 1};

  srcView = {
    minViewWidth: 100,
    minViewHeight: 100,
    initialHeight: 500,
    initialWidth: 500,
    fullscreenHeight: 700,
    fullscreenWidth: 700,
    minHeightPadding: 5,
    maxTextHeight: 20,
    minTextHeight: 10,
    sidePadding: 8,
    HWfactorPBM : 0.6666,
    HWfactorPGM : 2,
    HWfactorPPM : 6,
    highlightColor : {r: 0, g: 255, b: 255},
    highlightTime : 2000,
    highlightInterval : 200};

  editor = {
    imgActive: true,
    srcActive: true,
    color: {r: 255, g: 255, b: 255}
  };

  constructor(private http: Http) { }
  /**
   * Liest das JSON File ein. Prüft, ob die Werte vorhanden sind und liest diese dann ein.
   * Für jede Komponente oder Service wird eine eigene Funktion benutzt.
   * @param {Image} img Es kann ein leeres Startbild eingestellt werden, welches einem Callback übergeben wird.
   */
  init(imgSetter: (img: Image, color: any) => void) {

    const path = this.GET()['config'];
    console.log('Config Link: ' + path);
    this.http.get(path).subscribe(res => {
      this.isInit = true;
      this.data = res.json();
      if (this.data.hasOwnProperty('toolbar')) {
        this.checkToolbar(this.data.toolbar);
      }
      if (this.data.hasOwnProperty('newOverlay')) {
        this.checkOverlay(this.data.newOverlay);
      }
      if (this.data.hasOwnProperty('imgView')) {
        this.checkImgView(this.data.imgView);
      }
      if (this.data.hasOwnProperty('srcView')) {
        this.checkSrcView(this.data.srcView);
      }
      if (this.data.hasOwnProperty('editor')) {
        this.checkEditor(this.data.editor);
      }

      if (this.data.hasOwnProperty('imgAtStart')) {
        this.createImgData(this.data.imgAtStart, imgSetter);
      }
    });
  }

  /**
   * Parst die URL auf vorkommen von "?"
   */
  GET() {
    const vars = {};
    const parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value): string {
      vars[key] = value;
      return value;
    });
    return vars;
  }

  /* ---------------------------------------------------------------------------------------------------
   * Für jeden Service und Komponente werden hier die Einstellungen übernommen, welche angegeben wurden.
     ---------------------------------------------------------------------------------------------------*/

  checkToolbar(data: any) {
    if (!this.isInit) {
      return;
    }
    if (data.hasOwnProperty('showNew')) {
      this.toolbar.showNew = data.showNew;
    }
    if (data.hasOwnProperty('showFormat')) {
      this.toolbar.showFormat = data.showFormat;
    }
    if (data.hasOwnProperty('showFullScreen')) {
      this.toolbar.showFullScreen = data.showFullScreen;
    }
    if (data.hasOwnProperty('showSave')) {
      this.toolbar.showSave = data.showSave;
    }
    if (data.hasOwnProperty('showLoad')) {
      this.toolbar.showLoad = data.showLoad;
    }
    if (data.hasOwnProperty('showColorPicker')) {
      this.toolbar.showColorPicker = data.showColorPicker;
    }
    if (data.hasOwnProperty('showAsciiEnable')) {
      this.toolbar.showAsciiEnable = data.showAsciiEnable;
    }
    if (data.hasOwnProperty('showImageEnable')) {
      this.toolbar.showImageEnable = data.showImageEnable;
    }
    if (data.hasOwnProperty('showGridEnable')) {
      this.toolbar.showGridEnable = data.showGridEnable;
    }
    if (data.hasOwnProperty('toggleColor')) {
      this.toolbar.toggleColor = data.toggleColor;
    }
    if (data.hasOwnProperty('showUndo')) {
      this.toolbar.showUndo = data.showUndo;
    }
    if (data.hasOwnProperty('showRedo')) {
      this.toolbar.showRedo = data.showRedo;
    }
    if (data.hasOwnProperty('showTools')) {
      this.toolbar.showTools = data.showTools;
    }
  }

  checkOverlay(data: any) {
    if (!this.isInit) {
      return;
    }
    if (data.hasOwnProperty('maxWidth')) {
      this.overlay.maxWidth = data.maxWidth;
    }
    if (data.hasOwnProperty('maxHeight')) {
      this.overlay.maxHeight = data.maxHeight;
    }
    if (data.hasOwnProperty('initialName')) {
      this.overlay.initialName = data.initialName;
    }
    if (data.hasOwnProperty('initialWidth')) {
      this.overlay.initialWidth = data.initialWidth;
    }
    if (data.hasOwnProperty('initialHeight')) {
      this.overlay.initialHeight = data.initialHeight;
    }
  }

  checkImgView(data: any) {
    if (!this.isInit) {
      return;
    }
    if (data.hasOwnProperty('minViewWidth')) {
      this.imgView.minViewWidth = data.minViewWidth;
    }
    if (data.hasOwnProperty('initialWidth')) {
      this.imgView.initialWidth = data.initialWidth;
    }
    if (data.hasOwnProperty('initialHeight')) {
      this.imgView.initialHeight = data.initialHeight;
    }
    if (data.hasOwnProperty('fullscreenWidth')) {
      this.imgView.fullscreenWidth = data.fullscreenWidth;
    }
    if (data.hasOwnProperty('gridOn')) {
      this.imgView.gridOn = data.gridOn;
    }
    if (data.hasOwnProperty('gridStyle')) {
      this.imgView.gridStyle = data.gridStyle;
    }
    if (data.hasOwnProperty('lineWidthFactor')) {
      this.imgView.lineWidthFactor = data.lineWidthFactor;
    }
    if (data.hasOwnProperty('minLineWidth')) {
      this.imgView.minLineWidth = data.minLineWidth;
    }
  }

  checkSrcView(data: any) {
    if (!this.isInit) {
      return;
    }

    if (data.hasOwnProperty('minViewWidth')) {
      this.srcView.minViewWidth = data.minViewWidth;
    }
    if (data.hasOwnProperty('minViewHeight')) {
      this.srcView.minViewHeight = data.minViewHeight;
    }
    if (data.hasOwnProperty('initialWidth')) {
      this.srcView.initialWidth = data.initialWidth;
    }
    if (data.hasOwnProperty('initialHeight')) {
      this.srcView.initialHeight = data.initialHeight;
    }
    if (data.hasOwnProperty('fullscreenWidth')) {
      this.srcView.fullscreenWidth = data.fullscreenWidth;
    }
    if (data.hasOwnProperty('fullscreenHeight')) {
      this.srcView.fullscreenHeight = data.fullscreenHeight;
    }
    if (data.hasOwnProperty('minHeightPadding')) {
      this.srcView.minHeightPadding = data.minHeightPadding;
    }
    if (data.hasOwnProperty('maxTextHeight')) {
      this.srcView.maxTextHeight = data.maxTextHeight;
    }
    if (data.hasOwnProperty('minTextHeight')) {
      this.srcView.minTextHeight = data.minTextHeight;
    }
    if (data.hasOwnProperty('sidePadding')) {
      this.srcView.sidePadding = data.sidePadding;
    }
    if (data.hasOwnProperty('HWfactorPBM')) {
      this.srcView.HWfactorPBM = data.HWfactorPBM;
    }
    if (data.hasOwnProperty('HWfactorPGM')) {
      this.srcView.HWfactorPGM = data.HWfactorPGM;
    }
    if (data.hasOwnProperty('HWfactorPPM')) {
      this.srcView.HWfactorPPM = data.HWfactorPPM;
    }
    if (data.hasOwnProperty('highlightColor')) {
      this.srcView.highlightColor = data.highlightColor;
    }
    if (data.hasOwnProperty('highlightTime')) {
      this.srcView.highlightTime = data.highlightTime;
    }
    if (data.hasOwnProperty('highlightInterval')) {
      this.srcView.highlightInterval = data.highlightInterval;
    }
  }

  checkEditor(data: any) {
    if (!this.isInit) {
      return;
    }
    if (data.hasOwnProperty('imgActive')) {
      this.editor.imgActive = data.imgActive;
    }
    if (data.hasOwnProperty('srcActive')) {
      this.editor.srcActive = data.srcActive;
    }
    if (data.hasOwnProperty('color')) {
      this.editor.color = data.color;
    }
  }

  createImgData(data: any, imgSetter: (img: Image, color: any) => void) {
    if (!data.hasOwnProperty('name')) {
      return;
    }
    if (!data.hasOwnProperty('width')) {
      return;
    }
    if (!data.hasOwnProperty('height')) {
      return;
    }
    if (!data.hasOwnProperty('format')) {
      return;
    }
    const image = new Image(data.height, data.width);
    image.format = Number(data.format);
    image.name = data.name;
    imgSetter(image, this.editor.color);
  }
}
