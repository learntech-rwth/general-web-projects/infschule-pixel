import { Injectable } from '@angular/core';
import { Image } from '../shared/image';
import { ResizeEvent } from '../shared/events/resize-event';
import { ImgChangeEvent } from '../shared/events/img-change-event';
import { ColorElement } from '../shared/color-element';
import { ConfigStorageService } from '../services/config-storage.service';

interface RectPos {
  x: number;
  y: number;
}

/**
 * Dieser Service verwaltet das Rendern der Bildansicht und bietet Funktion für die
 * Ansicht an.
 */
@Injectable()
export class ImgRenderControllerService {
  curRect: RectPos;
  lastRect: RectPos;
  lastRectSelection: RectPos;
  canvas: HTMLCanvasElement;

  imgHeight = 500;
  imgWidth = 500;
  minViewWidth = 100;
  minViewHeigth = 100;
  pixelW = 0;
  pixelH = 0;
  toolMode = 0;
  isInit = false;
  img: Image;
  gridOn = false;
  rectSelection = false;

  constructor(private config: ConfigStorageService) {
    this.curRect = {x: 0, y: 0};
    this.lastRect = {x: 0, y: 0};
    this.lastRectSelection = {x: 0, y: 0};
    this.imgWidth = this.config.imgView.initialWidth;
    this.imgHeight = this.config.imgView.initialHeight;
    this.minViewWidth = this.config.imgView.minViewWidth;
  }

  /**
   * Der Controller muss für die meisten Funktionen erst Initialisiert werden.
   * @param {HTMLCanvasElement} canvas Das Canvas Objekt, auf welches sich die Funktionen Beziehen.
   * @param {Image} img Die Bild Daten, welche gezeichnet werden.
   * @returns Die änderung der Canvas größe.
   */
  init(canvas: HTMLCanvasElement, img: Image): ResizeEvent {
    this.isInit = true;
    this.img = img;
    this.canvas = canvas;
    const ctx = this.canvas.getContext('2d');
    this.gridOn = this.config.imgView.gridOn;
    return this.canvasResize();
  }

  /**
   * Berechnet die neue Höhe und den durchnschittlichen Pixelabstand.
   * @param {number} width Die Breite des Canvas Objekts.
   * @returns Die änderung der Canvas größe.
   */
  canvasResize(): ResizeEvent {
    if (!this.isInit) {
      return;
    }
    const width = this.imgWidth;
    const w = this.img.getwidth();
    const h = this.img.getheight();
    const oldHeight = this.imgHeight;
    const newHeight = (width / w) * h;
    this.canvas.width = width;
    this.canvas.height = newHeight;
    this.pixelW = this.precisionRound(width / w, 0);
    this.pixelH = this.precisionRound(newHeight / h, 0);
    return new ResizeEvent(0, newHeight - oldHeight);
  }

  /**
   * Zeichnet das Gitter, auf dem die "Pixel" gezeichnet werden
   */
  drawGrid() {
    if (!this.isInit) {
      return;
    }
    const ctx = this.canvas.getContext('2d');
    ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    ctx.strokeStyle = this.config.imgView.gridStyle;
    ctx.lineWidth = Math.min(this.pixelH * this.config.imgView.lineWidthFactor, this.config.imgView.minLineWidth);

    for (let i = 0; i < this.img.getwidth(); i++) {
      for (let j = 0; j < this.img.getheight(); j++) {
        ctx.beginPath();
        ctx.fillStyle = this.getFillColor(this.img.pixelList[i][j]);
        ctx.rect((i * this.pixelW), (j * this.pixelH), this.pixelW, this.pixelH);
        ctx.fill();
        if (this.gridOn) {
          ctx.stroke();
        }
      }
    }
  }

  /**
   * Selektiert das neue Pixel-Fenster, falls dieses sich geändert hat. Das alte wird
   * dabei gelöscht und wieder mit einem normalen Ramen versehen. Das Aktuell selektierte Fenster
   * wird im Render Controller gespeichert
   * @param {MouseEvent} event Daten, wo sich die Maus befindet und welche Taste gedrückt wurde.
   */
  rectSelect(event: MouseEvent) {
    if (!this.isInit) {
      return;
    }

    const x = this.getXFromEvent(event);
    const y = this.getYFromEvent(event);

    if (x >= this.img.getwidth() || y >= this.img.getheight()) {
      return;
    }

    if (x < 0 || y < 0) {
      return;
    }

    const ctx = this.canvas.getContext('2d');
    ctx.clearRect(this.lastRect.x * this.pixelW, this.lastRect.y * this.pixelH, this.pixelW, this.pixelH);
    ctx.beginPath();
    if (this.img.pixelList === undefined) {
      console.log('Pixellist is undefinded at ' + x + ':' + y);
    }
    if (this.img.pixelList[this.lastRect.x][this.lastRect.y] === undefined) {
      console.log('Colorpixel is undefinded at ' + x + ':' + y);
    }
    ctx.fillStyle = this.getFillColor(this.img.pixelList[this.lastRect.x][this.lastRect.y]);
    ctx.rect((this.lastRect.x * this.pixelW), (this.lastRect.y * this.pixelH), this.pixelW, this.pixelH);
    ctx.fill();
    if (this.gridOn) {
      ctx.strokeStyle = this.config.imgView.gridStyle;
      ctx.strokeRect(this.lastRect.x * this.pixelW, this.lastRect.y * this.pixelH, this.pixelW, this.pixelH);
    }
    this.drawSelect();
  }

  /**
   * Zeichnet das Selektionsrechteck, welches den aktuell gewählten Pixel zeigt.
   */
  drawSelect() {
    if (!this.isInit) {
      return;
    }
    if (!this.config.editor.imgActive) {
      return;
    }
    if (this.curRect.x < 0 || this.curRect.y < 0) {
      return;
    }
    const ctx = this.canvas.getContext('2d');
    ctx.strokeStyle = 'black';
    ctx.lineWidth = Math.min(this.pixelH * this.config.imgView.lineWidthFactor, this.config.imgView.minLineWidth);
    ctx.strokeRect((this.curRect.x * this.pixelW) + 2, (this.curRect.y * this.pixelH) + 2, this.pixelW - 4, this.pixelH - 4);
    ctx.strokeStyle = this.config.imgView.gridStyle;
  }

  /**
   * Zeichnet die Selektion im RechteckModus.
   * @param {MouseEvent} event Daten, wo sich die Maus befindet und welche Taste gedrückt wurde.
   */
  drawSelectRectangle(event: MouseEvent) {
    if (!this.rectSelection) {
      return;
    }
    const x = this.getXFromEvent(event);
    const y = this.getYFromEvent(event);

    if (x >= this.img.getwidth() || y >= this.img.getheight()) {
      return;
    }

    if (x < 0 || y < 0) {
      return;
    }


    // Es gibt die Positon das Rechteck beginnt und wo es hingezogen wird.
    // Abhängig der Positionen, muss der Rahmen an unterschiedlichen Ecken
    // der Pixel Anfangen, da der Selektionsrahmen Beide Pixel behinhalten muss.
    // curRect die Klick Position, x und y die aktuelle Position.
    const ctx = this.canvas.getContext('2d');
    ctx.strokeStyle = 'black';
    ctx.lineWidth = Math.min(this.pixelH * this.config.imgView.lineWidthFactor, this.config.imgView.minLineWidth);
    const x1 = this.curRect.x * this.pixelW;
    const x2 = x * this.pixelW;
    const y1 = this.curRect.y * this.pixelH;
    const y2 = y * this.pixelH;
    const width = Math.abs(x1 - x2) + this.pixelW;
    const height = Math.abs(y1 - y2) + this.pixelH;

    // Vier Fälle, Einer für jeden Quatranten, wenn man die Klickposition als Ursprung sieht.
    if (x >= this.curRect.x && y <= this.curRect.y) {
      ctx.strokeRect(x1 + 2, y2 + 2, width - 4, height - 4);
    } else if (x >= this.curRect.x && y > this.curRect.y) {
      ctx.strokeRect(x1 + 2, y1 + 2, width - 4, height - 4);
    } else if (x < this.curRect.x && y >= this.curRect.y) {
      ctx.strokeRect(x2 + 2, y1 + 2, width - 4, height - 4);
    } else if (x <= this.curRect.x && y < this.curRect.y) {
      ctx.strokeRect(x2 + 2, y2 + 2, width - 4, height - 4);
    }
    ctx.strokeStyle = this.config.imgView.gridStyle;
    this.lastRectSelection = {x: x, y: y};
  }

  /**
   * Berechnet die Pixel Position X.
   * @param {MouseEvent} event Daten, wo sich die Maus befindet und welche Taste gedrückt wurde.
   * @returns Die Pixel Position X.
   */
  private getXFromEvent(event: MouseEvent): number {
    const offsetX =  event.clientX - this.canvas.getBoundingClientRect().left;
    const x = Math.floor((offsetX + 1) / this.pixelW);
    return x;
  }

  /**
   * Berechnet die Pixel Position X.
   * @param {MouseEvent} event Daten, wo sich die Maus befindet und welche Taste gedrückt wurde.
   * @returns Die Pixel Position Y.
   */
  private getYFromEvent(event: MouseEvent): number {
    const offsetY = event.clientY - this.canvas.getBoundingClientRect().top;
    const y = Math.floor((offsetY + 1) / this.pixelH);
    return y;
  }

  /**
   * Prüft ob ein neues Feld erreicht wurde.
   * @param {MouseEvent} event Daten, wo sich die Maus befindet und welche Taste gedrückt wurde.
   * @returns True oder False.
   */
  checkNewField(event: MouseEvent): boolean {
    if (!this.isInit) {
      return;
    }
    const x = this.getXFromEvent(event);
    const y = this.getYFromEvent(event);

    if (this.curRect.x === x && this.curRect.y === y) {
      return false;
    }

    if (x >= this.img.getwidth() || y >= this.img.getheight()) {
      return false;
    }

    if (x < 0 || y < 0) {
      return false;
    }

    return true;
  }

  /**
   * An der aktuell gepeicherten Position ist eine änderung aufgetren.
   * @param {MouseEvent} event Daten, wo sich die Maus befindet und welche Taste gedrückt wurde.
   * @returns Die Funktion gibt die aktuele Position-en als Event zurück.
   */
  getImgChangeEvent(event: MouseEvent): ImgChangeEvent {
    if (!this.isInit) {
      return null;
    }
    let changeEvent: ImgChangeEvent;
    const x = this.getXFromEvent(event);
    const y = this.getYFromEvent(event);

    if (x >= this.img.getwidth() || y >= this.img.getheight()) {
      return null;
    }

    if (this.toolMode === 1) {
      let minX = Math.min(this.curRect.x, this.lastRectSelection.x);
      let minY = Math.min(this.curRect.y, this.lastRectSelection.y);
      minX = Math.max(minX, 0);
      minY = Math.max(minY, 0);
      const width = Math.abs(this.curRect.x - this.lastRectSelection.x) + 1;
      const height = Math.abs(this.curRect.y - this.lastRectSelection.y) + 1;
      changeEvent = new ImgChangeEvent(minX, minY, width, height);
    } else {
      changeEvent = new ImgChangeEvent(this.curRect.x, this.curRect.y);
    }

    return changeEvent;
  }

  /**
   * Wählt für das Rendern, abhängig des Formates, die passende Funktion für die Pixelfarbe.
   * @param {ColorElement} color Das Pixelelement.
   * @returns Die Farbe als "RGB(r,g,b)" String.
   */
  private getFillColor(color: ColorElement): string {
    if (this.img.format === 0) {
      return color.getRGB();
    } else if (this.img.format === 1) {
      return color.getGray();
    } else if (this.img.format === 2) {
      return color.getBlack();
    } else {
      return 'rgb(255,255,255);';
    }
  }

  /**
   * Setzt den Toolmodus für die Selektion.
   */
  setToolmode(mode: number) {
    this.toolMode = mode;
  }

  /**
   * Gibt den Toolmodus zurück.
   */
  getToolmode(): number {
    return this.toolMode;
  }

  /**
   * Gibt die Farbe als Event zurück.
   * @returns gibt die Farbe des aktuell anvisierten Pixels zurück.
   */
  getColor(event: MouseEvent): ImgChangeEvent {
    if (!this.isInit) {
      return null;
    }
    const ctx = this.canvas.getContext('2d');
    const offsetX =  event.clientX - this.canvas.getBoundingClientRect().left;
    const offsetY = event.clientY - this.canvas.getBoundingClientRect().top;
    const data = ctx.getImageData(offsetX, offsetY, 1, 1).data;
    const color = {r: Number(data[0]), g: Number(data[1]), b: Number(data[2])};
    const changeEvent = new ImgChangeEvent(offsetX, offsetY);
    changeEvent.color = color;
    return changeEvent;
  }

  /**
   * Setzt den Start dür die Rechteckselektion
   * @returns Gibt die Änderung des start Pixels zurück.
   */
  startRectangle(event: MouseEvent): ImgChangeEvent {
    if (!this.isInit) {
      return null;
    }
    const x = this.getXFromEvent(event);
    const y = this.getYFromEvent(event);

    if (x >= this.img.getwidth() || y >= this.img.getheight()) {
      return null;
    }

    this.lastRectSelection = {x: x, y: y};
    this.rectSelection = true;
    const changeEvent = this.getImgChangeEvent(event);
    if (!changeEvent) {
      return null;
    }
    changeEvent.state = 0;
    return changeEvent;
  }

  /**
   * Setzt die Aktuelle Pixelposition und speichert diese ab.
   */
  setCurRect(event: MouseEvent) {
    if (!this.isInit) {
      return;
    }
    const x = this.getXFromEvent(event);
    const y = this.getYFromEvent(event);

    if (this.curRect.x >= 0 && this.curRect.y >= 0) {
      this.lastRect = this.curRect;
    }
    this.curRect = {x: x, y: y};
  }

  /**
   * Prüft, ob gerade ein Rechteck selektiert wird.
   */
  endRectangle() {
    this.rectSelection = false;
  }

  /**
   * Der Fokus wird nicht mehr gezeichnet.
   */
  mouseLeave() {
    if (this.curRect.x >= 0 && this.curRect.y >= 0) {
      this.lastRect = this.curRect;
    }
    this.curRect = {x: -1, y: -1};
  }

  /**
   * Rundet eine Zahl.
   * @param {number} number Die Zahl.
   * @param {number} precision Auf welche stelle gerundet werden soll.
   */
  private precisionRound(number: number, precision: number): number {
    const factor = Math.pow(10, precision);
    return Math.round(number * factor) / factor;
  }

  /**
   * Die größe muss bei der Bild-Ansicht, den Verhältnissen angepasst werden
   * @param {ResizeEvent} event Die größe, umd die das Objekt verschoben werden muss.
   */
  adjustSize(event: ResizeEvent) {
    if (!this.isInit) {
      return;
    }
    const x = Math.max(event.width, event.height);
    const y = x * (this.img.getheight() / this.img.getwidth());
    event.setSize(x, y);
  }
}
