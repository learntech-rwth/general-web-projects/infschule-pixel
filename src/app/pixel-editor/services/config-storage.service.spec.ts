import { TestBed, inject } from '@angular/core/testing';

import { ConfigStorageService } from './config-storage.service';

describe('ConfigStorageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConfigStorageService]
    });
  });

  it('should be created', inject([ConfigStorageService], (service: ConfigStorageService) => {
    expect(service).toBeTruthy();
  }));
});
