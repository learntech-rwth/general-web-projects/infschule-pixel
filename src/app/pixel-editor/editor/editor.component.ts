import { Component, OnInit, EventEmitter, ElementRef, ViewChild, HostListener } from '@angular/core';
import { ControllerService } from '../services/controller.service';
import { EditorEvent } from '../shared/editor-event';
import { ResizeEvent } from '../shared/events/resize-event';
import { EventBase } from '../shared/events/event-base';
import { ImgChangeEvent } from '../shared/events/img-change-event';
import { SrcChangeEvent } from '../shared/events/src-change-event';

import { ConfigStorageService } from '../services/config-storage.service';
import { ColorPickerModule } from '../../color-picker/color-picker.module';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {

  @ViewChild('canvasContainer') canvasContainer: ElementRef;
  @ViewChild('rowContainer') rowContainer: ElementRef;

  constructor(public controller: ControllerService,
    private config: ConfigStorageService) {
  }

  ngOnInit() {
    const rContainer = this.rowContainer.nativeElement;
    const cContainer = this.canvasContainer.nativeElement;
    this.controller.setCollisionObjects(rContainer, cContainer);
    this.controller.setMinSize(100);
  }

  /**
   * Die Funktion wertet alle Events der Symbolleiste aus und ruft die entsprechende Funktionen auf.
   * @param { EditorEvent} event Enhält alle Informationen über das auftretende Event.
   */
  topToolbarEvent(event: EditorEvent) {
    if (event.eventNumber === 1) {
      this.controller.eventNew();
    } else if (event.eventNumber === 2) {
      this.controller.eventSave();
    } else if (event.eventNumber === 3) {
      this.controller.eventLoad(event.payload);
    } else if (event.eventNumber === 4) {
    } else if (event.eventNumber === 5) {
      this.controller.eventColor(event);
    } else if (event.eventNumber === 6) {
      this.controller.eventNewFormat(event.payload);
    } else if (event.eventNumber === 7) {
      this.controller.eventImgView(event.payload);
    } else if (event.eventNumber === 8) {
      this.controller.eventSrcView(event.payload);
    } else if (event.eventNumber === 9) {
      this.controller.eventGrid(event.payload);
    } else if (event.eventNumber === 10) {
      this.controller.fullscreenImg();
    } else if (event.eventNumber === 11) {
      this.controller.fullscreenSrc();
    } else if (event.eventNumber === 12) {
      this.controller.eventPencil();
    } else if (event.eventNumber === 13) {
      this.controller.eventRectangle();
    } else if (event.eventNumber === 14) {
      this.controller.eventRubber();
    } else if (event.eventNumber === 15) {
      this.controller.eventPipette();
    } else if (event.eventNumber === 16) {
      this.controller.eventUndo();
    } else if (event.eventNumber === 17) {
      this.controller.eventRedo();
    }
  }

  eventHandlerImg(event: EventBase) {
    switch (event.eventNumber) {
      case 0: { // Neue Größe
        this.controller.imgSize = this.controller.resize(<ResizeEvent>event);
        break;
      }
      case 1: {
        if (this.config.editor.imgActive) { // Änderung im Bild
          this.controller.imgChange(<ImgChangeEvent>event);
        }
      }
    }
  }

  eventHandlerSrc(event: EventBase) {
    switch (event.eventNumber) {
      case 0: { // Neue Größe
        this.controller.srcSize = this.controller.resize(<ResizeEvent>event);
        break;
      }
      case 1: { // Änderung im Bild
        if (this.config.editor.srcActive) {
          this.controller.srcChange(<SrcChangeEvent>event);
        }
      }
    }
  }

  colorPickerClose(color: {r: number, g: number, b: number}) {
    this.controller.colorPickerClose(color);
  }

  newColor(color: {r: number, g: number, b: number}) {
    this.controller.newColor(color);
  }

  @HostListener('document:keydown', ['$event']) onkeydown(event: KeyboardEvent) {
    if (event.keyCode === 27) {
      this.controller.endFullscreen();
    }
  }
}
