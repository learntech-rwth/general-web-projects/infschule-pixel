import { Component, OnInit, AfterViewInit, AfterViewChecked} from '@angular/core';
import { Output, Input, EventEmitter, ElementRef, ViewChild, HostListener} from '@angular/core';
import { colorInputValidator, blackInputValidator } from '../shared/max-size-validator';
import { SrcRenderControllerService } from '../services/src-render-controller.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ResizeEvent } from '../shared/events/resize-event';
import { Image } from '../shared/image';
import { EventBase } from '../shared/events/event-base';

@Component({
  selector: 'app-src-view',
  templateUrl: './src-view.component.html',
  styleUrls: ['./src-view.component.css']
})

/**
 * Die Komponente, welche zuständig ist für die Quellcode-Ansicht des Bildes. Die Logik befindet sich
 * im Renderer-Service. Hier werden die Eingaben und registriert und das Aussehen gesteuert
 */
export class SrcViewComponent implements OnInit, AfterViewInit, AfterViewChecked {
  srcHeight = 500;
  srcWidth = 500;
  showInput = false;
  colorFormat = false;
  FocusEmitter = new EventEmitter<boolean>();
  init = false;
  height: string;
  width: string;
  canvas: HTMLCanvasElement;
  inputBox: FormGroup;
  inputBoxs: FormGroup;
  setFocus = false;
  inputStyle = {left: '10px', top: '10px', width: '0px', height: '0px', position: 'absolute'};

  @ViewChild('srccanvas') elCanvas: ElementRef;
  @ViewChild('form') elForm: ElementRef;
  @ViewChild('container') elContainer: ElementRef;

/**
 * Ändert die Größe der Komponente bzw. des Canvas.
 * @param {ResizeEvent} size Die Größe in einer relativen Änderung oder Absolut.
 */
  @Input() set newSize(size: ResizeEvent){
    if (!size) {
      return;
    }
    this.srcHeight += size.height;
    this.srcWidth += size.width;
    this.height = this.srcHeight + 'px';
    this.width = this.srcWidth + 'px';
    if (!size) {
      return;
    }
    if (size.isRelative) {
      this.renderer.srcHeight += size.height;
      this.renderer.srcWidth += size.width;
    } else {
      this.renderer.srcWidth = size.width;
      this.renderer.srcHeight = size.height;
    }
    if (this.renderer.srcWidth < this.renderer.minViewWidth) {
      this.renderer.srcWidth = this.renderer.minViewWidth;
    }
    if (this.renderer.srcHeight < this.renderer.minViewHeight) {
      this.renderer.srcHeight = this.renderer.minViewHeight;
    }
    this.height = this.renderer.srcHeight + 'px';
    this.width = this.renderer.srcWidth + 'px';
    this.renderer.reConfig();
    this.renderer.draw();
  }

/**
 * Initialisiert die Anzeige bei einem neuen Bild.
 * @param {Image} img Das neue Bild. Eine neue Referenz muss gespeichert werden.
 */
  @Input() set newImg(img: Image) {
    if (!img) {
      return;
    }
    this.renderer.init(<HTMLCanvasElement>this.elCanvas.nativeElement, img);
    this.renderer.draw();
    this.init = true;
  }

  @Output() eventTrigger = new EventEmitter<EventBase>();

  constructor(private renderer: SrcRenderControllerService) {
    this.height = this.srcHeight + 'px';
    this.width = this.srcWidth + 'px';
  }

  /**
   * Die FormGroups. Der Inhalt ist egal, da diese nicht Sichtbar sind.
   */
  ngOnInit() {
    this.inputBox = new FormGroup({
      'input': new FormControl('', [
        Validators.required,
        colorInputValidator(255)
      ]),
    });
    this.inputBoxs = this.getInputBoxsFormGroup();
  }

  getInputBoxsFormGroup(): FormGroup {
    return new FormGroup({
      'r': new FormControl('', [Validators.required, colorInputValidator(255)]),
      'g': new FormControl('', [Validators.required, colorInputValidator(255)]),
      'b': new FormControl('', [Validators.required, colorInputValidator(255)]),
    });
  }

  /**
   * Das der Renderer darf kann erst richtig Konfigutiert werden, wenn der View aufgebaut wurde.
   * Die Funktion wird aufgerufen, sobald der View zum ersten mal gerendert wurde.
   */
  ngAfterViewInit() {
    this.renderer.reConfig();
    this.renderer.draw();
  }

  /**
   * Die Funktion wird nach jedem View Check aufgerufen. Wenn das Inputfeld aktiviert wurde,
   * wird der Fokus darauf gelegt. (Dafür muss das Input-Feld existieren)
   */
  ngAfterViewChecked() {
    if (this.setFocus) {
      this.setFocus = false;
      this.FocusEmitter.emit(true);
    }
    return;
  }

  /**
   * Prüft bei der (R|G|B) eingabe, ob diese gültig ist und löst dann ein srcChangeEvent aus.
   */
  checkInputs() {
    if (this.inputBoxs.get('r').invalid) {
      return;
    }
    if (this.inputBoxs.get('g').invalid) {
      return;
    }
    if (this.inputBoxs.get('b').invalid) {
      return;
    }
    const r = this.inputBoxs.get('r').value;
    const g = this.inputBoxs.get('g').value;
    const b = this.inputBoxs.get('b').value;
    this.eventTrigger.emit(this.renderer.getChangeEvent(r, g, b));
  }

  /**
   * Prüft bei der Eingabe, ob diese gültig ist und löst dann ein srcChangeEvent aus.
   * Hier muss zusätzlich noch geprüft werden, ob es sich um ein Datenfeld oder einen Farbwert handelt.
   * @param {any} event Das KeyEvent des letzten Tastendrucks.
   */
  checkInput(event: any) {
    if (this.inputBox.get('input').invalid) {
      return;
    }
    const changeEvent = this.renderer.checkInput(event);
    if (changeEvent != null) {
      this.eventTrigger.emit(changeEvent);
    }
    // this.FocusEmitter.emit(false);
    // this.showInput = 'hidden';
  }

  /**
   * Wird von der Resize Komponente aufgerufen wenn daran gezogen wird.
   * @param {ResizeEvent} event Gibt die verschiebung an.
   */
  resize(event: ResizeEvent) {
    this.eventTrigger.emit(event);
    this.showInput = false;
  }

  lostFocus() {
  }

  /**
   * Achtet auf Globale Klicks (Damit das Input Feld auch wieder geschlossen werden kann) und wertet die
   * Eingabe aus. Es wird ein Eingabefeld erzeugt oder wieder deaktiviert.
   */
  @HostListener('document:click', ['$event'])
  onmouseclick(event: MouseEvent) {
    if (!this.init) {
      return;
    }
    if (!this.elContainer.nativeElement.contains(<Element>event.target)) {
      this.showInput = false;
      return;
    }

    if (this.showInput && this.colorFormat && (<HTMLElement>this.elForm.nativeElement).contains(event.toElement)) {
      return;
    }
    const style = this.renderer.drawInpuBox(event);
    if (style === null) {
      this.showInput = false;
      return;
    }
    this.inputStyle = style;
    this.inputBox = this.renderer.getFormGroup();
    delete this.inputBoxs;
    this.inputBoxs = this.getInputBoxsFormGroup();
    this.showInput = true;
    this.colorFormat = this.renderer.isColorFormat();
    this.setFocus = true;
  }

  /**
   * Ein HostListener für einen Rechtsklick. Ändert den Farbwert des ausgewählten Pixels auf den
   * letzten eingegebenen.
   */
  @HostListener('mousedown', ['$event']) onmousedown(event: MouseEvent) {
    if (!this.init) {
      return;
    }
    if (event.button === 2) {
      this.showInput = false;
      if (!this.renderer.setInputPixelFocus(event)) {
        return;
      }
      this.eventTrigger.emit(this.renderer.getChangeEventVoid());
      return;
    }
  }

  /**
   * HostListener für die Entertaste. Die Eingabe wird beendet.
   */
  @HostListener('keydown', ['$event']) onkeydown(event: KeyboardEvent) {
    if (event.keyCode === 13) {
      this.showInput = false;
    }
  }
}
