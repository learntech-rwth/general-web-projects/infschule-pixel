import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SrcViewComponent } from './src-view.component';

describe('SrcViewComponent', () => {
  let component: SrcViewComponent;
  let fixture: ComponentFixture<SrcViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SrcViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SrcViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
