import { Component, HostListener, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { ResizeEvent } from '../shared/events/resize-event';

@Component({
  selector: 'app-resize',
  templateUrl: './resize.component.html',
  styleUrls: ['./resize.component.css']
})

/**
 * Teilt der Parent-Komponente, bei einem Drag, die relative Änderung mit.
 */
export class ResizeComponent implements OnInit {
  // Speichert, um es sich um einen Zug dieser Komponente handelt.
  click = false;
  // Die StartPosition des Klicks
  initX: number;
  initY: number;

  @Output() resizeEvent = new EventEmitter<ResizeEvent>();
  @Output() setResize = new EventEmitter<boolean>();

  constructor(private _el: ElementRef) { }

  ngOnInit() { }

  @HostListener('mousedown', ['$event']) onmousedown(event: MouseEvent) {
    this.click = true;
    this.initX = event.clientX;
    this.initY = event.clientY;
    this.setResize.emit(true);
  }

  @HostListener('document:mousemove', ['$event']) onmousemove(event: MouseEvent) {
    if (!this.click) {
      return;
    }

    const resizeEvent = new ResizeEvent;
    resizeEvent.setSize(event.clientX - this.initX, event.clientY - this.initY);
    this.resizeEvent.emit(resizeEvent);
    this.initX = event.clientX;
    this.initY = event.clientY;
  }

  @HostListener('document:mouseup') onmouseup() {
    if (!this.click) {
      return true;
    }
    this.setResize.emit(false);
    this.click = false;
    return true;
  }
}



